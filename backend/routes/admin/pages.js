var express = require('express');
var router = express.Router();

router.get('/', async function(req, res, next) {
  const response = await req.app.db.Page.findAll({include: req.app.db.PageSetting, order: [
    ['id', 'asc'],
    [req.app.db.PageSetting, 'order', 'asc']
  ]});
  res.json(response);
});

router.put('/settings/:pageSettingID', async  function(req, res, next) {
  const response = await req.app.db.PageSetting.update({...req.body},{where:{
    id: req.params.pageSettingID
} });
  res.json(response)
});

router.post('/settings/', async function(req, res, next) {
  // modifier un pages
  const count = await req.app.db.PageSetting.count({ where: {'PageId': req.body.PageId } });
  const response = await req.app.db.PageSetting.create({...req.body, order: count +1});
  res.json(response)
});

router.delete('/settings/:pageSettingID', async function(req, res, next) {
  const response = await req.app.db.PageSetting.destroy({where:{
    id: req.params.pageSettingID
} });
  res.json(response)
});


router.put('/:pageID', async  function(req, res, next) {
  const response = await req.app.db.Page.update({...req.body, id: undefined},{where:{
    id: req.params.pageID
} });
  res.json(response)
});

router.post('/', async function(req, res, next) {
  // modifier un pages
  const response = await req.app.db.Page.create({...req.body});
  res.json(response)
});

router.delete('/:pageID', async function(req, res, next) {
  const response = await req.app.db.Page.destroy({where:{
    id: req.params.pageID
} });
  res.json(response)
});
module.exports = router;
