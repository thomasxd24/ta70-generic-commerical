var express = require('express');
var router = express.Router();

router.get('/', async function(req, res, next) {
  const response = await req.app.db.Subscription.findAll();
  res.json(response);
});

router.put('/:id', async  function(req, res, next) {
  const response = await req.app.db.Subscription.update({...req.body, id: undefined},{where:{
    id: req.params.id
} });
  res.json(response)
});

router.post('/', async function(req, res, next) {
  // modifier un pages
  const response = await req.app.db.Subscription.create({...req.body});
  res.json(response)
});

router.delete('/:id', async function(req, res, next) {
  const response = await req.app.db.Subscription.destroy({where:{
    id: req.params.id
} });
  res.json(response)
});
module.exports = router;
