var express = require('express');
var router = express.Router();

router.get('/', async function(req, res, next) {
  const response = await req.app.db.FAQ.findAll();
  res.json(response);
});

router.put('/:id', async  function(req, res, next) {
  const response = await req.app.db.FAQ.update({...req.body, id: undefined},{where:{
    id: req.params.id
} });
  res.json(response)
});

router.post('/', async function(req, res, next) {
  // modifier un pages
  const count = await req.app.db.FAQ.count();
  const response = await req.app.db.FAQ.create({...req.body, order: count +1});
  res.json(response)
});

router.delete('/:id', async function(req, res, next) {
  const response = await req.app.db.FAQ.destroy({where:{
    id: req.params.id
} });
  res.json(response)
});
module.exports = router;
