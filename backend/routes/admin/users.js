var express = require('express');
var router = express.Router();

router.get('/', async function(req, res, next) {
  const response = await req.app.db.User.findAll();
  res.json(response);
});

router.put('/:id', async  function(req, res, next) {
  const response = await req.app.db.User.update({...req.body, id: undefined},{where:{
    id: req.params.id
} });
  res.json(response)
});

router.post('/', async function(req, res, next) {
  // modifier un pages
  const response = await req.app.db.User.create({...req.body});
  res.json(response)
});

router.delete('/:id', async function(req, res, next) {
  const response = await req.app.db.User.destroy({where:{
    id: req.params.id
} });
  res.json(response)
});
module.exports = router;
