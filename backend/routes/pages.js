var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', async function(req, res, next) {
  const response = await req.app.db.Page.findAll({include: req.app.db.PageSetting, order: [
    ['id', 'asc'],
    [req.app.db.PageSetting, 'order', 'asc']
  ]});
  res.json(response);
});

router.get('/faq', async function(req, res, next) {
  const response = await req.app.db.FAQ.findAll();
  res.json(response);
});

module.exports = router;
