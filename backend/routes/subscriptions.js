var express = require('express');
var router = express.Router();

/* GET subscriptions listing. */
router.get('/', async function (req, res, next) {
    // send subscription activate
    let offers = await req.app.db.Subscription.findAll({
        where: {
            enable: true,
        }
    });
    res.send(offers);
});

/* GET the user subscription */
router.get('/get', async function (req, res, next) {
    let user = await req.app.db.User.findOne({
        where: {
            id: req.user.id
        }
    });

    if(user) {
        let result = await req.app.db.Subscription.findOne({
            where: {
                id: user.SubscriptionId,
            }
        }).catch(err => {
            console.log(err);
            res.send("Error to modify subscription of user");
        });
        result.dataValues.dateOfSubscription = user.subscribeAt;
        res.send(result.dataValues);
    }
    else
        res.send("Error the user haven't subscription")
});

/* PUT link user to subscription */
router.put('/subscribe/:idSubscription', async function (req, res, next) {
    // send subscription activate
    if(!req.user)
    {
        res.status(401);
        return res.send('None shall pass');
    }
    const {idSubscription} = req.params;
    var date = new Date();
    date.setDate(date.getDate() + 30);
    await req.app.db.User.update({SubscriptionId: idSubscription, subscribeAt: new Date(), unsubscribeAt: date}, {
        where: {
            id: req.user.id
        }
    }).catch(err => console.log(err));
    res.send("ok");
});

router.put('/unsubscribe', async function (req, res, next) {
    // send subscription activate
    if(!req.user)
    {
        res.status(401);
        return res.send('None shall pass');
    }

    await req.app.db.User.update({SubscriptionId: null, unsubscribeAt: req.app.db.sequelize.fn('datetime', 'now')}, {
        where: {
            id: req.user.id
        }
    }).catch(err => console.log(err));
    res.send("ok");
});




module.exports = router;
