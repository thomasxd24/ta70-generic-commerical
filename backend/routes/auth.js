var express = require('express');
var router = express.Router();
var passport = require('passport')
const { body, validationResult } = require('express-validator');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/signup', 
 body('email').isEmail(),
 body('password').isLength({ min: 5 }),
 body('firstName').notEmpty(),
 body('lastName').notEmpty(),
  async function(req, res) {
    // Finds the validation errors in this request and wraps them in an object with handy functions
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    var {email, firstName, lastName, password } = req.body
    await req.app.db.User.create({email, firstName, lastName, password })
    res.send("OK")
  });
router.post('/login', 
  passport.authenticate('local'),
  function(req, res) {
    res.redirect('/api/users');
  });

  router.get('/apple',
  passport.authenticate('apple'));

  router.post('/apple',
  express.urlencoded(),
  passport.authenticate('apple'),
  (req, res) => {
    // Successful authentication, redirect home.
    res.redirect('/');
  });

router.get('/google',
  passport.authenticate('google', { scope: ['profile', 'email'] }));

  router.get('/google/callback', 
  passport.authenticate('google'),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  });
  router.get('/logout', function(req, res){
    req.logout();
    res.send("Logged out")
  });

module.exports = router;
