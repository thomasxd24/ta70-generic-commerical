var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send("<a href='https://ta70.ttse.me/api/auth/login'>Sign in with Apple</a>");
});

module.exports = router;
