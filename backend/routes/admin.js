var express = require('express');
var router = express.Router();
var subscriptionRouter = require("./admin/subscriptions")
var faqRouter = require("./admin/faq")
var pagesRouter = require("./admin/pages")
var userRouter = require("./admin/users")

router.use('/subscriptions', subscriptionRouter);
router.use('/faq', faqRouter);
router.use('/pages', pagesRouter);
router.use('/users', userRouter);
module.exports = router;
