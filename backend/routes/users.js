var express = require('express');
const passport = require('passport');
var router = express.Router();
const UserService = require("../services/users.service")
const {body, validationResult} = require("express-validator");

/* GET users listing. */
router.get('/',async function(req, res, next) {
  // repondre avec l'objet de user courant
  if(!req.user)
  {
    res.status(401);
    return res.send('None shall pass');
  }
  var user = await req.app.db.User.findOne({ where: { id: req.user.id },
    include: [req.app.db.Subscription, req.app.db.Card]}).catch((err)=>{console.error(err); return null});

  res.json(user);
});
//user update profile
router.put('/', async function(req, res) {
    if(!req.user)
    {
        res.status(401);
        return res.send('None shall pass');
    }
    var {firstName, lastName, email, address, phone } = req.body
    await req.app.db.User.update({firstName: firstName, lastName: lastName, email: email, address: address, phone: phone }, {
          where: {
              id: req.user.id
          }
    }).catch(err => console.log(err));
    res.send("OK")
});

router.post('/subscription', async function(req, res, next) {
  // commander avec l'abonnement selectionee
  // var response = await UserService.orderSubscription(req);
  res.send(response);
});

router.delete('/subscription', async function(req, res, next) {
  // resilier l'abonnement actuelle
  // var reponse = await UserService.cancelSubscription(req);
  res.send('respond with a resource');
});

router.delete('/', async function(req, res, next) {
  var card = await req.app.db.User.destroy({where: {id: req.user.id}});
  res.send('respond with a resource');
});


router.get('/invoices', async function(req, res, next) {
  // recuperer tous les factures
  // var reponse = await UserService.cancelSubscription(req);
  res.send('respond with a resource');
});

router.get('/cards/all', async function (req, res, next) {
    // recuperer une card de paiement
    // var reponse = await UserService.addCard(req);
    //var card = await req.app.db.Card.findOne({where: {UserId: req.user.id}});
    var card = await req.app.db.Card.findAll();
    res.json(card);
});


router.post('/cards', async function (req, res, next) {
    // ajouter une card de paiement
    // var reponse = await UserService.addCard(req);
    if(!req.user)
    {
        res.status(401);
        return res.send('None shall pass');
    }
    var { numberCard, ownerCard, expirationDate, cvc} = req.body
    var user = await req.app.db.User.findOne({where: {
      id: req.user.id
  }});
    var card = user.createCard({ numberCard, ownerCard, expirationDate, cvc})

    res.json(card);
});

router.delete('/cards', async function (req, res, next) {
    // supprimer un carte de paiement
    // var reponse = await UserService.deleteCard(req)
    if(!req.user)
    {
        res.status(401);
        return res.send('None shall pass');
    }
    var user = await req.app.db.User.findOne({where: {
      id: req.user.id
  }});
    if(!user.CardId) return res.json({error: "No Card saved"});
    var card = await req.app.db.Card.destroy({where: {id: user.CardId}})
    res.json(card);
});

module.exports = router;
