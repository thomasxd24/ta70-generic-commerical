const BaseService = require("./base.servcice")

class UserService extends BaseService {
    async createUser(req) {
        var error;
        await req.app.db.User.create({
                email: req.user.email, 
                firstName: req.user.firstName, 
                lastName: req.user.lastName, 
                password: req.user.password
            }).catch(err => error = err);
        return {
            error: error
        }
    }

    async updateUser(req){
        var error;
        await req.app.db.User.update({
            firstName: req.user.firstName, 
            lastName: req.user.lastName, 
            email: req.user.email, 
            address: req.user.address, 
            phone: req.user.phone }, {
            where: {
                id: req.user.id
            }
      }).catch(err => error = err);
      return {
          error: error
      }
    }
    
    async getUser(req) {
        return {
            result: json(await req.app.db.User.findOne(
                {
                    where: {
                        id: req.user.id
                    },
                    include: [
                        req.app.db.Subcription,
                        req.app.db.Card
                    ]
                }))
        }
    }

    async orderSubscription(req) {
        var error;
        await req.app.db.User.update({SubscriptionId: req.subcription.id, subscribeAt: req.app.db.sequelize.fn('datetime', 'now')}, {
            where: {
                id: req.user.id
            }
        }).catch(err => error = err);
        return {
            error: error
        }
    }

    async cancelSubscription(req) {
        var error;
        var result = await req.app.db.User.update({SubscriptionId: null, unsubscribeAt: req.app.db.sequelize.fn('datetime', 'now')}, {
            where: {
                id: req.user.id
            }
        }).catch(err => error = err);
        return {
            result: result,
            error: error
        }
    }


    async listInvoices(req) {
        var error;
        var result = await req.app.db.Invoice.findAll({
            where: {
                User : req.user.id
            }
        }, {include: [ User ]})
        return {
            result: "a faire"
        }
    }

    async addCard(req) {
        var error;
        await req.app.db.Card.create({
            numberCard: req.card.numberCard,
            ownerCard: req.card.ownerCard,
            expirationDate: req.card.expirationDate,
            ccv: req.card.ccv,
            User: {id: req.user.id}
        },{include: [ User ]}
        ).catch(err => error = err);

        return {
            error: error
        }
    }


    async deleteCard(req) {
        var error;
        await req.app.db.Card.destroy({
            where: {User: {id: req.user.id}}}, 
            {include: [ User ]}).catch(err => error = err);
        return {
            error: error
        }
    }
}

module.exports = UserService