const UserService = require("./users.service");
const SubscriptionService = require("./subscription.service");

function getServices(db) {
    return {
        user : new UserService(),
        subscription: new SubscriptionService()
    }
}