"use strict";
const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            models.User.hasMany(models.Invoice)
            models.User.belongsTo(models.Subscription)
            models.User.belongsTo(models.Card)
        }
    }

    User.init(
        {
            firstName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            lastName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            address: {
                type: DataTypes.STRING,
            },
            phone: {
                type: DataTypes.NUMBER,
            },
            admin: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            subscribeAt: {
                type: DataTypes.DATE,
                allowNull: true
            },
            unsubscribeAt: {
                type: DataTypes.DATE,
                allowNull: true
            },
            enable: {
                type: DataTypes.BOOLEAN,
                defaultValue: true,
            },
        },
        {
            sequelize,
            modelName: "User",
            defaultScope: {
                attributes: {exclude: ['password']},
            }
        }
    );
    return User;
};
