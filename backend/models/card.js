"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Card extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
    }
  }
  Card.init(
    {
        // Model attributes are defined here
        numberCard: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        ownerCard: {
            type: DataTypes.STRING,
            allowNull: false
        },
        expirationDate: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cvc: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    },
    {
      sequelize,
      modelName: "Card"
    }
  );
  return Card;
};

