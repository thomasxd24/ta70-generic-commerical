"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PageSetting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.PageSetting.belongsTo(models.PageSetting);
    }
  }
  PageSetting.init(
    {
      // Model attributes are defined here
      title: {
        type: DataTypes.STRING,
        allowNull: false
      },
      order: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false
      },
      imageURL: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      sequelize,
      modelName: "PageSetting"
    }
  );
  return PageSetting;
};