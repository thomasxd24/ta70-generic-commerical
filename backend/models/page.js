
"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Page extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Page.hasMany(models.PageSetting);
    }
  }
  Page.init(
    {
      // Model attributes are defined here
      title: {
          type: DataTypes.STRING,
          allowNull: false
      },
      path: {
        type: DataTypes.STRING,
        allowNull: false
    },
    header: {
      type: DataTypes.STRING,
      allowNull: false
  },
      description: {
        type: DataTypes.STRING,
          allowNull: true
      }

  },
    {
      sequelize,
      modelName: "Page"
    }
  );
  return Page;
};


