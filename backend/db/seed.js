module.exports = {
    initSeed : async (db) =>  {
      if(!await db.Subscription.count() != 0) {
  // insert data in database
  await db.Subscription.create({
    title: "Free",
    link: "https://google.com",
    prices: 0,
    description: "Essential needed\n30 days\nLimited support",
    enable: true,
  });
  
  await db.Subscription.create({
    title: "Starter",
    link: "https://google.com",
    prices: 9.99,
    description: "Essential needed\n30 days\nLimited support",
    enable: true,
  });
  
  await db.Subscription.create({
    title: "Premium",
    link: "https://google.com",
    prices: 19.99,
    description: "Essential needed\n30 days\nLimited support",
    enable: true,
  })
  await db.Subscription.create({
    title: "Ultimate",
    link: "https://google.com",
    prices: 59.99,
    description: "Essential needed\n30 days\nLimited support",
    enable: true,
  })
      }
        
  

  if(!await db.Page.count() != 0) {
    await db.Page.create({
      title: "Acceuil",
      path: "/",
      header: "Serveur VPS",
      description: "Nous vous proposons d’acquerir un VPS avec une connexion rapide à un prix défiant tout concurrence"
    })
    await db.Page.create({
      title: "Offres",
      path: "/offers",
      header: "Nos Abonnement",
      description: "Nous vous proposons d’acquerir un VPS avec une connexion rapide à un prix défiant tout concurrence"
    })
    await db.Page.create({
      title: "À propos",
      path: "/about",
      header: "Qui somme-nous",
      description: "Nous vous proposons d’acquerir un VPS avec une connexion rapide à un prix défiant tout concurrence"
    })
    await db.Page.create({
      title: "FAQ",
      path: "/help",
      header: "Foire au questions",
      description: "Nous vous proposons d’acquerir un VPS avec une connexion rapide à un prix défiant tout concurrence"
    })
    await db.Page.create({
      title: "Contact",
      path: "/contact",
      header: "Contactez-nous",
      description: "Veuillez entrer les détails de votre demande. Un membre de notre équipe de support vous répondra dans les plus brefs délais."
    })
  }

  if(!await db.FAQ.count() != 0) {
    await db.FAQ.create({
      question: "Comment souscrire un abonnement NordVPN ?",
      answer: "La meilleure façon est de souscrire directement sur notre site Internet. Nous proposons des prix NordVPN imbattables dans le cadre de nos offres spéciales. Il vous suffit de faire défiler vers le haut cette page, de choisir l’abonnement qui correspond à vos besoins et de cliquer sur “choisir l'abonnement”. Dans la page de commande, entrez simplement votre adresse électronique, sélectionnez votre mode de paiement et terminez la transaction.",
      order: 1,
    })
    await db.FAQ.create({
      question: "Combien coûte un VPN ?",
      answer: "La plupart des fournisseurs de VPN proposent des abonnements mensuels et quelques autres à long terme. Les utilisateurs doivent généralement payer pour toute la période d'achat d'un abonnement. Les abonnements de deux et trois ans représentent naturellement les meilleures offres. Les abonnements mensuels coûtent de 6,80 €-11,05 €/mois, tandis que les abonnements annuels sont le plus souvent proposés entre 5,10 €-5,95 €/mois.",
      order: 1,
    })
    await db.FAQ.create({
      question: "Combien coûte un VPN ?",
      answer: "La plupart des fournisseurs de VPN proposent des abonnements mensuels et quelques autres à long terme. Les utilisateurs doivent généralement payer pour toute la période d'achat d'un abonnement. Les abonnements de deux et trois ans représentent naturellement les meilleures offres. Les abonnements mensuels coûtent de 6,80 €-11,05 €/mois, tandis que les abonnements annuels sont le plus souvent proposés entre 5,10 €-5,95 €/mois.",
      order: 1,
    })
    await db.FAQ.create({
      question: "Combien coûte un VPN ?",
      answer: "La plupart des fournisseurs de VPN proposent des abonnements mensuels et quelques autres à long terme. Les utilisateurs doivent généralement payer pour toute la période d'achat d'un abonnement. Les abonnements de deux et trois ans représentent naturellement les meilleures offres. Les abonnements mensuels coûtent de 6,80 €-11,05 €/mois, tandis que les abonnements annuels sont le plus souvent proposés entre 5,10 €-5,95 €/mois.",
      order: 1,
    })
    await db.FAQ.create({
      question: "Combien coûte un VPN ?",
      answer: "La plupart des fournisseurs de VPN proposent des abonnements mensuels et quelques autres à long terme. Les utilisateurs doivent généralement payer pour toute la période d'achat d'un abonnement. Les abonnements de deux et trois ans représentent naturellement les meilleures offres. Les abonnements mensuels coûtent de 6,80 €-11,05 €/mois, tandis que les abonnements annuels sont le plus souvent proposés entre 5,10 €-5,95 €/mois.",
      order: 1,
    })
    await db.FAQ.create({
      question: "Combien coûte un VPN ?",
      answer: "La plupart des fournisseurs de VPN proposent des abonnements mensuels et quelques autres à long terme. Les utilisateurs doivent généralement payer pour toute la période d'achat d'un abonnement. Les abonnements de deux et trois ans représentent naturellement les meilleures offres. Les abonnements mensuels coûtent de 6,80 €-11,05 €/mois, tandis que les abonnements annuels sont le plus souvent proposés entre 5,10 €-5,95 €/mois.",
      order: 1,
    })
  }

  const userAccount = await db.User.count();

  if(userAccount == 0) {
    await db.User.create({
      firstName: "Admin",
      lastName: "Blackpink",
      email: "admin@utbm.fr",
      password: "admin",
      address: "Rue des rue",
      phone: "0123456889",
      admin: true
    })
  }
  

    }
}