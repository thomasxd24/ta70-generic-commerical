var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var db = require("./models")
var indexRouter = require('./routes/index');
var authRouter = require('./routes/auth');
var userRouter = require('./routes/users');
var pageRouter = require('./routes/pages');
var userContact = require('./routes/contact');

var subscriptionsRouter = require('./routes/subscriptions');
var adminRouter = require('./routes/admin');
var cors = require('cors')
var session = require("express-session")

var passport = require('passport');
const jwt_decode = require("jwt-decode") ;
var app = express();
app.use(express.static('../storefront/dist'));

var  bodyParser = require("body-parser");

const seed = require('./db/seed');
const auth = require('./config/auth');



app.use(logger('dev'));
app.use(express.json());
app.use(cors({credentials: true, origin: function (origin, callback) {
  callback(null, origin)
}}))
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../storefront/public')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ secret: "cats" , resave: false,
saveUninitialized: true,}));
auth.initPassport(app, db)
app.use(passport.initialize());
app.use(passport.session());
app.testop = true;

app.db = db;
app.db.sequelize.sync().then(()=>{console.log("All models were synchronized successfully.");seed.initSeed(db);});


app.use('/api', indexRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/contact', userContact);
app.use('/api/pages', pageRouter);
app.use('/api/subscription', subscriptionsRouter);
app.use('/api/admin', adminRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send( err.message);
});

module.exports = app;
