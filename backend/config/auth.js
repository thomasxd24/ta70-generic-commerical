
var GoogleStrategy = require('passport-google-oauth20').Strategy;
const AppleStrategy = require('@nicokaiser/passport-apple').Strategy;
var LocalStrategy = require('passport-local').Strategy;
var passport = require('passport');
const fs = require('fs');
var path = require('path');
module.exports  = {
    initPassport: (app) => {
        passport.use(new LocalStrategy({
            usernameField: 'email'
          },
            async function(email, password, done) {
          // implement logic here
          const user = await app.db.User.findOne({
            where: { email: email, password: password },
          });
          if(user)  return done(null, user);
              return done(null, false, { message: 'Incorrect email/password.' });
            }
          ));
          
          
          
          passport.use(new GoogleStrategy({
              clientID: "777164601821-kap597ht1mlcp0dd10k9k6fqbdlda0ni.apps.googleusercontent.com",
              clientSecret: "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",
              callbackURL: "https://ta70.ttse.me/api/auth/google/callback"
            },
            async function(accessToken, refreshToken, profile, cb) {
            const [user, created] = await app.db.User.findOrCreate({
              where: { email: profile.emails[0].value },
              defaults: {
                lastName: profile.name?.familyName,
                firstName: profile.name?.givenName,
                password: "fsodjnosbkg"
              }
            });
            return cb(null, user);
            }
          ));
          
          passport.use(new AppleStrategy({
            clientID: 'fr.utbm.ta70', // Services ID
            teamID: '65K4BP7824', // Team ID of your Apple Developer Account
            keyID: '926Z79TCD7', // Key ID, received from https://developer.apple.com/account/resources/authkeys/list
            key: fs.readFileSync(path.join('config', 'AuthKey.p8')), // Private key, downloaded from https://developer.apple.com/account/resources/authkeys/list
            scope: ['name', 'email'],
            callbackURL: 'https://ta70.ttse.me/api/auth/apple'
          },
          async (accessToken, refreshToken, profile, cb) => {
            // name is avaliable at account creation
            // email is avaliable all the time
            const [user, created] = await app.db.User.findOrCreate({
              where: { email: profile.email },
              defaults: {
                lastName: profile.name?.lastName ?? "Hello",
                firstName: profile.name?.firstName ?? "Bois",
                password: "fsodjnosbkg"
              }
            });
            return cb(null, user);
          }
          ));
          
          
          passport.serializeUser(function(user, done) {
            done(null, user);
          });
          
          passport.deserializeUser(function(user, done) {
            done(null, user);
          });
    }
}