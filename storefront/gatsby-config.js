module.exports = {
  siteMetadata: {
    title: "gatsby-starter-medusa",
    version: "1.0.0",
  },
  proxy: {
    prefix: "/api",
    url: "http://10.1.0.120:3000",
  },
  plugins: [
    "gatsby-plugin-image",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
  ],
};
