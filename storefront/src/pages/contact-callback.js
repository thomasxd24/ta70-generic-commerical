import React, { useContext, useState } from "react";
import { FaGithub } from "react-icons/fa";
import { graphql } from "gatsby";
import * as styles from "../styles/home.module.css";
import * as formStyles from "../styles/contact.module.css";
import { Link } from "gatsby";
import { Button } from 'semantic-ui-react';


// markup
const AboutPage = ({ data }) => {
    const [isSubmitted, setSubmitted] = useState(false);
     return (
       <div className={styles.container}>
         <main className={styles.main}>
            <div className={styles.hero}>
                <br/>
                <div className={formStyles.centeredText}>
                    <h1 className={styles.title}>Contact</h1>

                    <h1>Formulaire envoyé !</h1>

                    <p>
                    L’équipe du support traite votre demande et reviendra vers vous sous peu de temps.
                    </p>

                    <p>
                    Bonne continuation.
                    </p>

                    <br/>

                    <Button as={Link} to="/" primary size='large'>Retourner à l’acceuil</Button>
                </div>
            </div>
         </main>
       </div>
     );
   };
   
   export default AboutPage;


//    const AboutPage = ({ data }) => {
//     const [isSubmitted, setSubmitted] = useState(false);
//      return (
//        <div className={styles.container}>
//          <main className={styles.main}>
//          {isSubmitted ? (<p>contactform</p>) : (<p>confirmform</p>)}
//            <button onClick={() => setSubmitted(!isSubmitted)}>
//              Cliquez ici
//            </button>
//          </main>
//        </div>
//      );
//    };
   
//    export default AboutPage;