import React, { useContext, useState, useReducer } from "react";
import { FaGithub } from "react-icons/fa";
import { graphql, navigate } from "gatsby";
import * as styles from "../styles/home.module.css";
import * as formStyles from "../styles/contact.module.css";
import { Link } from "gatsby";
import { Input, Button, Checkbox, TextArea, Form, Select, Container } from 'semantic-ui-react';
import contactService from "../services/contact.service";
import HeaderTitle from "../components/home/headerTitle"
import PageContext from "../context/pageContext";

const ContactForm = ({ data }) => {

  const pages = useContext(PageContext);
  const contactSetting = pages.find(page => page.title == "Contact")
  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      topic: "",
      subject: "",
      description: "",
      firstName: "",
      name: "",
      email: "",
      phoneNumber: "",
      country: ""
    }
  );

  const handleOnChange = event => {
    const { name, value } = event.target;
    setInputValues({ [name]: value });
  };

  const handleOnChangeDropdown = (name, value) => {
    setInputValues({ [name]: value });
  };

  const handleContact = async () => {
    await contactService.postFormData(inputValues);
    navigate("/contact-callback");
  }

  return (
    <div>
      <HeaderTitle boxTitle={{title: contactSetting?.header, description: contactSetting?.description, hideButton: true}}></HeaderTitle>
      <Container style={{marginTop: "2rem"}}>
          <p>
            <span className={formStyles.redText}>* Champ obligatoire</span>
          </p>
          <Form>
            <Form.Field required> 
              <label>En quoi pouvons-nous vous aider ? </label>
              <Select fluid
                value={inputValues.topic}
                onChange={(evt)=>handleOnChangeDropdown("topic", evt.target.textContent)}
                name="topic"
                
                options={themes}
                placeholder="Choisir le thème"
                id="topic"
              />
            </Form.Field>

            <Form.Field required>
              <label>Sujet </label>
              <Input
                value={inputValues.subject}
                onChange={handleOnChange}
                
                name="subject"
                id="subject"
                placeholder=''
              />
            </Form.Field>

            <Form.Field required>
              <label>Description </label>
              <TextArea rows={8}
                value={inputValues.description}
                onChange={handleOnChange}
                name="description"
                
                style={{
                  minHeight: 100,
                  maxHeight: 300
                }}
                id="description"
                placeholder=''
              />
            </Form.Field>

            <Form.Group widths='equal'>
              <Form.Field required> 
                <label>Prénom </label>
                <Input
                  value={inputValues.firstName}
                  onChange={handleOnChange}
                  
                  name="firstName"
                  id="firstName"
                  placeholder=''
                />
              </Form.Field>

              <Form.Field required>
                <label>Nom </label>
                <Input
                  value={inputValues.name}
                  onChange={handleOnChange}
                  
                  name="name"
                  id="name"
                  placeholder=''
                />
              </Form.Field>
            </Form.Group>

            <Form.Field required>
              <label>Adresse e-mail </label>
              <Input fluid
                value={inputValues.email}
                
                onChange={handleOnChange}
                name="email"
                id="email"
                placeholder=''
              />
            </Form.Field>

            <Form.Field>
              <label>Téléphone (optionnel)</label>
              <Input fluid
                value={inputValues.phoneNumber}
                onChange={handleOnChange}
                
                name="phoneNumber"
                id="phoneNumber"
                placeholder=''
              />
            </Form.Field>

            <Form.Field required>
              <label>Pays/zone géographique</label>
              <Select fluid
                value={inputValues.country}
                onChange={(evt)=>handleOnChangeDropdown("country", evt.target.textContent)}
                name="country"
                options={counrties}
                placeholder='Choisir le pays'
                id="country"
              />
            </Form.Field>

            <Form.Field required>
              <Checkbox 
              label='La Politique de confidentialité (+ lien vers politique de confidentialité)' />
            </Form.Field>

            <Form.Field required>
              <Checkbox 
                label="Conditions d'utilisation (+ lien vers  les CGU)" />
            </Form.Field>
            <div className={formStyles.centeredText}>
              <Button primary
                onClick={handleContact}
                size='large'
                type='submit'>Envoyer</Button>
            </div>
          </Form>
      </Container>
      
    </div>
    
  )
};



const themes = [
  { key: 'a', text: 'J\'ai besoin de contacter un employé ou un service en particulier', value: 'J\'ai besoin de contacter un employé ou un service en particulier' },
  { key: 'b', text: 'J\'ai une question en particulier', value: 'J\'ai une question en particulier' },
  { key: 'c', text: 'J\'ai besoin de conseils sur un service', value: 'J\'ai besoin de conseils sur un service' },
  { key: 'd', text: 'J\'ai des questions concernant un abonnement', value: 'J\'ai des questions concernant un abonnement' },
  { key: 'e', text: 'J\'ai un problème avec un de mes abonnements', value: 'J\'ai un problème avec un de mes abonnements' },
];

const counrties = [
  { key: 'af', value: 'Afghanistan', text: 'Afghanistan' },
  { key: 'ax', value: 'Aland Islands', text: 'Aland Islands' },
  { key: 'al', value: 'Albania', text: 'Albania' },
  { key: 'dz', value: 'Algeria', text: 'Algeria' },
  { key: 'as', value: 'American Samoa', text: 'American Samoa' },
  { key: 'ad', value: 'Andorra', text: 'Andorra' },
  { key: 'ao', value: 'Angola', text: 'Angola' },
  { key: 'ai', value: 'Anguilla', text: 'Anguilla' },
  { key: 'ag', value: 'Antigua', text: 'Antigua' },
  { key: 'ar', value: 'Argentina', text: 'Argentina' },
  { key: 'am', value: 'Armenia', text: 'Armenia' },
  { key: 'aw', value: 'Aruba', text: 'Aruba' },
  { key: 'au', value: 'Australia', text: 'Australia' },
  { key: 'at', value: 'Austria', text: 'Austria' },
  { key: 'az', value: 'Azerbaijan', text: 'Azerbaijan' },
  { key: 'bs', value: 'Bahamas', text: 'Bahamas' },
  { key: 'bh', value: 'Bahrain', text: 'Bahrain' },
  { key: 'bd', value: 'Bangladesh', text: 'Bangladesh' },
  { key: 'bb', value: 'Barbados', text: 'Barbados' },
  { key: 'by', value: 'Belarus', text: 'Belarus' },
  { key: 'be', value: 'Belgium', text: 'Belgium' },
  { key: 'bz', value: 'Belize', text: 'Belize' },
  { key: 'bj', value: 'Benin', text: 'Benin' },
];


export default ContactForm;
