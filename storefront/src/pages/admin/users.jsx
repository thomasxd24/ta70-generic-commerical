
import React from "react";
import {graphql} from "gatsby";
// import * as styles from "../styles/home.module.css";
import {Button, Container, Grid, Header, Image, Segment, Accordion, Icon, Table } from "semantic-ui-react"
import { useState } from "react";
import ModalPage from "../../components/dialogs/modalPage";
import { useEffect } from "react";
import adminService from "../../services/admin.service";
import ModalPageSetting from "../../components/dialogs/modalPageSetting";
import ModalUser from "../../components/dialogs/modalUser";

const UsersSettingsPage = ({}) => {
    const [activeIndex, setActiveIndex] = useState(null);
    const [users, setUsers] = useState([])
    const handleClick = (e, titleProps) => {
        const { index } = titleProps
        const newIndex = activeIndex === index ? -1 : index
        setActiveIndex(newIndex)
      }
      const handleClickDelete = async (userID) => {
        const result = window.confirm("Etes-vous sur de supprimer l'utilisateur??");
        if(!result) return;
        const response = await adminService.deleteUser(userID);
        fetchUsers();
      }
      const fetchUsers = async () => {
          const response = await adminService.getUsers();
          setUsers(response)
      }
    useEffect(() => {
      fetchUsers()
    },[])


    return (
        <div>
            <h2>Utilisateurs</h2>
            <ModalUser handleRefresh={fetchUsers}></ModalUser>
            <Table celled>
    <Table.Header>
    <Table.Row>
        <Table.HeaderCell>Nom</Table.HeaderCell>
        <Table.HeaderCell>Prenom</Table.HeaderCell>
        <Table.HeaderCell>Email</Table.HeaderCell>
        <Table.HeaderCell>Admin</Table.HeaderCell>
        <Table.HeaderCell>Abonnement actuel</Table.HeaderCell>
        <Table.HeaderCell>Action</Table.HeaderCell>
      </Table.Row>
    
      
    </Table.Header>

    <Table.Body>
    {users?.map(user => (<Table.Row>
        <Table.Cell>{user.firstName}</Table.Cell>
        <Table.Cell>{user.lastName}</Table.Cell>
        <Table.Cell>{user.email}</Table.Cell>
        <Table.Cell>{user.admin ? "Oui":"Non"}</Table.Cell>
        <Table.Cell>{user.Subscription?.name ?? "Non Abonné"}</Table.Cell>
        <Table.Cell><Button icon color="red" onClick={() => {handleClickDelete(user.id)}}> <Icon name="trash"></Icon></Button></Table.Cell>
      </Table.Row>))}
      
      
    </Table.Body>
  </Table>
            
            
        </div>
    )
}


export default UsersSettingsPage;