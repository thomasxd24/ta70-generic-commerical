
import React from "react";
import {graphql} from "gatsby";
// import * as styles from "../styles/home.module.css";
import {Button, Container, Grid, Header, Image, Segment, Accordion, Icon, Table } from "semantic-ui-react"
import { useState } from "react";
import ModalPage from "../../components/dialogs/modalPage";
import { useEffect } from "react";
import adminService from "../../services/admin.service";
import ModalPageSetting from "../../components/dialogs/modalPageSetting";
import ModalUser from "../../components/dialogs/modalUser";
import ModalFAQ from "../../components/dialogs/modalFAQ";

const FAQSettingsPage = ({}) => {
    const [activeIndex, setActiveIndex] = useState(null);
    const [faqs, setFAQ] = useState([])
    const handleClick = (e, titleProps) => {
        const { index } = titleProps
        const newIndex = activeIndex === index ? -1 : index
        setActiveIndex(newIndex)
      }
      const handleClickDelete = async (faqID) => {
        const result = window.confirm("Etes-vous sur de supprimer l'utilisateur??");
        if(!result) return;
        const response = await adminService.deleteFAQ(faqID);
        fetchUsers();
      }
      const fetchUsers = async () => {
          const response = await adminService.getFAQ();
          setFAQ(response)
      }
    useEffect(() => {
      fetchUsers()
    },[])


    return (
        <div>
            <h2>Abonnements</h2>
            <ModalFAQ handleRefresh={fetchUsers}></ModalFAQ>
            <Table celled>
    <Table.Header>
    <Table.Row>
        <Table.HeaderCell>Question</Table.HeaderCell>
        <Table.HeaderCell>Réponse</Table.HeaderCell>
        <Table.HeaderCell>Action</Table.HeaderCell>
      </Table.Row>
    
      
    </Table.Header>

    <Table.Body>
    {faqs?.map(faq => (<Table.Row>
        <Table.Cell>{faq.question}</Table.Cell>
        <Table.Cell>{faq.answer}</Table.Cell>
        <Table.Cell><Button icon color="red" onClick={() => {handleClickDelete(faq.id)}}> <Icon name="trash"></Icon></Button></Table.Cell>
      </Table.Row>))}
      
      
    </Table.Body>
  </Table>
            
            
        </div>
    )
}


export default FAQSettingsPage;