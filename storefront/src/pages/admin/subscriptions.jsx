
import React from "react";
import {graphql} from "gatsby";
// import * as styles from "../styles/home.module.css";
import {Button, Container, Grid, Header, Image, Segment, Accordion, Icon, Table } from "semantic-ui-react"
import { useState } from "react";
import ModalPage from "../../components/dialogs/modalPage";
import { useEffect } from "react";
import adminService from "../../services/admin.service";
import ModalPageSetting from "../../components/dialogs/modalPageSetting";
import ModalUser from "../../components/dialogs/modalUser";
import ModalSubscription from "../../components/dialogs/modalSubscription";

const SubscriptionSettingsPage = ({}) => {
    const [activeIndex, setActiveIndex] = useState(null);
    const [subscriptions, setSubscriptions] = useState([])
    const handleClick = (e, titleProps) => {
        const { index } = titleProps
        const newIndex = activeIndex === index ? -1 : index
        setActiveIndex(newIndex)
      }
      const handleClickDelete = async (subscriptionID) => {
        const result = window.confirm("Etes-vous sur de supprimer l'utilisateur??");
        if(!result) return;
        const response = await adminService.deleteSubscription(subscriptionID);
        fetchUsers();
      }
      const fetchUsers = async () => {
          const response = await adminService.getSubscriptions();
          setSubscriptions(response)
      }
    useEffect(() => {
      fetchUsers()
    },[])


    return (
        <div>
            <h2>Abonnements</h2>
            <ModalSubscription handleRefresh={fetchUsers}></ModalSubscription>
            <Table celled>
    <Table.Header>
    <Table.Row>
        <Table.HeaderCell>Titre</Table.HeaderCell>
        <Table.HeaderCell>Prix</Table.HeaderCell>
        <Table.HeaderCell>Lien de service</Table.HeaderCell>
        <Table.HeaderCell>Action</Table.HeaderCell>
      </Table.Row>
    
      
    </Table.Header>

    <Table.Body>
    {subscriptions?.map(subscription => (<Table.Row>
        <Table.Cell>{subscription.title}</Table.Cell>
        <Table.Cell>{subscription.prices}</Table.Cell>
        <Table.Cell>{subscription.link}</Table.Cell>
        <Table.Cell><Button icon color="red" onClick={() => {handleClickDelete(subscription.id)}}> <Icon name="trash"></Icon></Button></Table.Cell>
      </Table.Row>))}
      
      
    </Table.Body>
  </Table>
            
            
        </div>
    )
}


export default SubscriptionSettingsPage;