
import React from "react";
import {graphql} from "gatsby";
// import * as styles from "../styles/home.module.css";
import {Button, Container, Grid, Header, Image, Segment, Accordion, Icon, Table } from "semantic-ui-react"
import { useState } from "react";
import ModalPage from "../../components/dialogs/modalPage";
import { useEffect } from "react";
import adminService from "../../services/admin.service";
import ModalPageSetting from "../../components/dialogs/modalPageSetting";

const PagesSettingsPage = ({}) => {
    const [activeIndex, setActiveIndex] = useState(null);
    const [pages, setPages] = useState([])
    const handleClick = (e, titleProps) => {
        const { index } = titleProps
        const newIndex = activeIndex === index ? -1 : index
        setActiveIndex(newIndex)
      }


      const handleClickDeleteSetting = async (pageSettingID) => {
        const result = window.confirm("Etes-vous sur de supprimer la contenu?");
        if(!result) return;
        const response = await adminService.deletePageSetting(pageSettingID);
        fetchPages();
      }
      const handleClickDelete = async (pageID) => {
        const result = window.confirm("Etes-vous sur de supprimer la page?");
        if(!result) return;
        const response = await adminService.deletePage(pageID);
        fetchPages();
      }
      const fetchPages = async () => {
          const response = await adminService.getPages();
          setPages(response)
      }
    useEffect(() => {
        fetchPages()
    },[])


    return (
        <div>
            <h2>Pages/Contenu des pages</h2>
            <ModalPage handleRefresh={fetchPages}></ModalPage>
            
            <Accordion styled fluid>
                {pages.map((page, index) => (
                    <div key={index}>
                <Accordion.Title
          active={activeIndex === index}
          index={index}
          onClick={handleClick}
        >
          <Icon name='dropdown' />
          {page.title}
        </Accordion.Title>
        <Accordion.Content active={activeIndex === index}>
        
          <p>
            <b>Header:</b> {page.header} <br></br>
            <b>Description:</b> {page.description}<br></br>
            <b>Chemin d'acces:</b> {page.path}<br></br>
          </p>
          <div>
           <ModalPageSetting pageID={page.id} handleRefresh={fetchPages} />
            <ModalPage originalPage={page} handleRefresh={fetchPages} ></ModalPage>
            <Button onClick={() => handleClickDelete(page.id)} color="red">Supprimer la page</Button>
        </div>
        <h4>Contenu du page</h4>
        <Table celled >
    <Table.Header>
      <Table.Row> 
        <Table.HeaderCell  width="4">Titre</Table.HeaderCell>
        <Table.HeaderCell>Description</Table.HeaderCell>
        <Table.HeaderCell width="3">URL du image</Table.HeaderCell>
        <Table.HeaderCell collapsing>Action</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
        {page.PageSettings.map((setting, indexSetting) => (<Table.Row key={indexSetting}>
        <Table.Cell>
         {setting.title}
        </Table.Cell>
        <Table.Cell>{setting.description}</Table.Cell>
        <Table.Cell>{setting.imageURL}</Table.Cell>
        <Table.Cell>
            <ModalPageSetting originalPage={setting} pageID={page.id} handleRefresh={fetchPages} />
            <Button color="red" onClick={() => handleClickDeleteSetting(setting.id)} icon><Icon name="trash"></Icon></Button>
        </Table.Cell>
      </Table.Row>))}
      
      
    </Table.Body>
  </Table>
        </Accordion.Content>
                </div>
                ))}
      </Accordion>
        </div>
    )
}


export default PagesSettingsPage;