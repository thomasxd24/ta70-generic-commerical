import React, {useContext, useEffect, useState} from "react";
import * as styles from "../../styles/account.module.css";
import { Input, Button, Checkbox, TextArea, Form, Select, Container  } from 'semantic-ui-react';
import Menu from "../../components/account/menu";
import NordVpn from "../../images/logo-Nordvpn.jpg";
import authService from "../../services/auth.service"
import userService from "../../services/user.service";
import {navigate} from "gatsby";
import {globalHistory} from "@reach/router";
import {  toast } from 'react-toastify';
import LayoutAccount from "../../components/account/layoutAccount";
import UserContext from "../../context/userContext"

// markup
const InformationPage = ({data}) => {

    const userRoot = useContext(UserContext)

    let currentUser = {
        firstName: "",
        lastName: "",
        email: "",
        phone: "",
        address: ""
    };

    const [user, setUser] = useState(currentUser);

    useEffect(() => {
        setUser(userRoot ?? currentUser)
    }, [userRoot]);

    function setField(field, value) {
        setUser({...user, [field]: value})
    }

    function sendUser() {
        userService.modifUser(user).then(r => toast.success("Utilisateur modifiee"));
    }

    async function deleteUser() {
        const result = window.confirm("Etes-vous sur de vouloir cluturer votre compte?");
        if(!result) return;
        await userService.deleteUser();
        navigate("/")
    }

    return (
        <LayoutAccount>
            <Form className={styles.width} styles={{paddingTop: "10px"}}>
                        <Form.Group widths='equal'>
                            <Form.Field className={styles.inputGroup}>
                                <label>Prénom</label>
                                <Input id="first-name" value={user.firstName}
                                       onChange={e => setField("firstName", e.target.value)}/>
                            </Form.Field>
                            <Form.Field className={styles.inputGroup}>
                                <label>Nom</label>
                                <Input id="name" value={user.lastName}
                                       onChange={e => setField("lastName", e.target.value)}/>
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Form.Field className={styles.inputGroup}>
                                <label>Adresse e-mail</label>
                                <Input fluid id="email" value={user.email}
                                       onChange={e => setField("email", e.target.value)}/>
                            </Form.Field>
                            <Form.Field className={styles.inputGroup}>
                                <label>Adresse</label>
                                <Input fluid id="email" value={user.address}
                                       onChange={e => setField("address", e.target.value)}/>
                            </Form.Field>
                        </Form.Group>
                        <Form.Field className={styles.input}>
                            <label>Numéro de téléphone</label>
                            <Input id="telephone" value={user.phone} onChange={e => setField("phone", e.target.value)}/>
                        </Form.Field>
                        <div className={styles.flexBtw}>
                            <Button primary size='large' type='submit' onClick={sendUser}>Enregistrer les infos</Button>
                            <Button color='red' size='large' onClick={() => deleteUser() }>Cloture le compte</Button>
                        </div>
                    </Form>
        </LayoutAccount>
    );
};

const countries = [
    { key: 'af', value: 'af', text: 'Afghanistan' },
    { key: 'ax', value: 'ax', text: 'Aland Islands' },
    { key: 'al', value: 'al', text: 'Albania' },
    { key: 'dz', value: 'dz', text: 'Algeria' },
    { key: 'as', value: 'as', text: 'American Samoa' },
    { key: 'ad', value: 'ad', text: 'Andorra' },
    { key: 'ao', value: 'ao', text: 'Angola' },
    { key: 'ai', value: 'ai', text: 'Anguilla' },
    { key: 'ag', value: 'ag', text: 'Antigua' },
    { key: 'ar', value: 'ar', text: 'Argentina' },
    { key: 'am', value: 'am', text: 'Armenia' },
    { key: 'aw', value: 'aw', text: 'Aruba' },
    { key: 'au', value: 'au', text: 'Australia' },
    { key: 'at', value: 'at', text: 'Austria' },
    { key: 'az', value: 'az', text: 'Azerbaijan' },
    { key: 'bs', value: 'bs', text: 'Bahamas' },
    { key: 'bh', value: 'bh', text: 'Bahrain' },
    { key: 'bd', value: 'bd', text: 'Bangladesh' },
    { key: 'bb', value: 'bb', text: 'Barbados' },
    { key: 'by', value: 'by', text: 'Belarus' },
    { key: 'be', value: 'be', text: 'Belgium' },
    { key: 'bz', value: 'bz', text: 'Belize' },
    { key: 'bj', value: 'bj', text: 'Benin' },
]

export default InformationPage;
