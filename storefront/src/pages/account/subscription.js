import React, { useContext } from "react";
import * as styles from "../../styles/account.module.css";
import {Input, Button, Checkbox, TextArea, Form, Select, Container, Icon} from 'semantic-ui-react';
import Menu from "../../components/account/menu";
import LayoutAccount from "../../components/account/layoutAccount";
import UserContext from "../../context/userContext";

// markup
const SubscriptionPage = ({ data }) => {
    
    const user = useContext(UserContext)
    return (
        <LayoutAccount>
            {
                user?.Subscription ? (
                    <div>
                    <h2>Abonnement {user.Subscription.title}</h2>
                    <p>Date de dernier facture : {new Date(user.subscribeAt).toDateString()}</p>
                    <p>Date du prochain facture : {new Date(user.unsubscribeAt).toDateString()}</p>
                    <p>Prix : {user.Subscription.prices}€</p>
                    <p style={{ whiteSpace: 'pre' }}>Avantages : <br/>
                    {user.Subscription.description}
                        
                    </p>
                    <Button color='red' size='large'>Résillier d’abonnement</Button>
                    </div>
                   
                ): (<div>
<h3>Vous n'avez aucun abonnement en cours. </h3>
<Button size='large' primary>Decourvir les offres</Button>
                </div>)
            }
          
        </LayoutAccount>
    );
};

export default SubscriptionPage;
