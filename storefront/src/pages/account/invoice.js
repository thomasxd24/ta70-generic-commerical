import React, { useContext } from "react";
import * as styles from "../../styles/account.module.css";
import {Input, Button, Checkbox, TextArea, Form, Select, Container, Icon} from 'semantic-ui-react';
import Menu from "../../components/account/menu";
import LayoutAccount from "../../components/account/layoutAccount";

// markup
const InvoicePage = ({ data }) => {

  return (
      <LayoutAccount>
          <h1>Mes dernières factures :</h1>
                  <div className={styles.factures}>
                      <Icon name='file pdf'/> <p> facture du 10/01/2022</p>
                  </div>
                  <div className={styles.factures}>
                      <Icon name='file pdf'/> <p> facture du 10/12/2021</p>
                  </div>
                  <div className={styles.factures}>
                      <Icon name='file pdf'/> <p> facture du 10/11/2021</p>
                  </div>
      </LayoutAccount>
  );
};

export default InvoicePage;
