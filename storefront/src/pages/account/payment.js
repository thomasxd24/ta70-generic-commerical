import React, {useContext, useEffect, useState} from "react";
import * as styles from "../../styles/account.module.css";
import {Input, Button, Checkbox, TextArea, Form, Select, Container, Message, Header, Confirm} from 'semantic-ui-react';
import Menu from "../../components/account/menu";
import LayoutAccount from "../../components/account/layoutAccount";
import authService from "../../services/auth.service";
import cardService from "../../services/card.service";
import userService from "../../services/user.service";
import UserContext from "../../context/userContext";
import {  toast } from 'react-toastify';

// markup
const PaymentPage = ({data}) => {
    const [open, setOpen] = useState(false);
    const user = useContext(UserContext)
    useEffect(()=> {
        setBC(user?.Card)
    },[user])

    const cardDefault = {
        ownerCard: "",
        numberCard: "",
        cvc: "",
        expirationDate: ""
    };

    const [BC, setBC] = useState(null);
    const setField = (field, value) => {
        setBC({...BC, [field]: value})
    }
    

    function sendCard(card) {
        cardService.modifCardUser(card).then(r => toast.success("Carte modifiee"));
    }

    function deleteCard() {
        cardService.deleteCardUser().then(r => {setBC(null); toast.success("Carte supprimee")});
    }

    function addCard() {
        setBC(cardDefault)
    }

  return (
      <LayoutAccount>
          <Header as="h3">Carte enregistrée:</Header>
          {
          BC ?
          (<div>
 <Form>
    <Form.Group widths='equal'>
        <Form.Field className={styles.inputGroup}>
            <label>Nom sur la carte</label>
            <Input type="text" id="name" value={BC.ownerCard} onChange={e => setField("ownerCard", e.target.value)}/>
        </Form.Field>
        <Form.Field className={styles.inputGroup}>
            <label>Numéro de la carte</label>
            <Input id="number" type="number" max="9999999999999999" value={BC.numberCard} onChange={e => setField("numberCard", e.target.value)}/>
        </Form.Field>
    </Form.Group>
    <Form.Group widths='equal'>
        <Form.Field className={styles.inputGroup}>
            <label>Code de sécuriter</label>
            <Input fluid id="code" type="password" maxlength="3" value={BC.cvc} onChange={e => setField("cvc", e.target.value)}/>
        </Form.Field>
        <Form.Field className={styles.inputGroup}>
            <label>Date d'expiration</label>
            <Input fluid id="date" type="text" value={BC.expirationDate}
                   onChange={e => setField("expirationDate", e.target.value)}/>
        </Form.Field>
    </Form.Group>
    <div className={styles.flexBtw}>
        <Button primary size='large' type='submit' onClick={() => sendCard(BC)} color="teal"  id="btnsuccessleft">Enregistrer la carte</Button>
        <Button color='red' size='large' onClick={() =>  setOpen(true)}>Supprimer la carte</Button>
    </div>
</Form>
<Confirm
          open={open}
          content='Etes vous sur de supprimer la carte? Cette action est ireeversible'
          onCancel={()=> setOpen(false)}
          onConfirm={()=> {setOpen(false); deleteCard()}}
        />
          </div>):(<div>
            <Header as="h4">Aucune carte n'est enregistrée</Header>
            <Button onClick={addCard}>Ajouter une carte</Button>
          </div>)}
        
      </LayoutAccount>
  );
};

export default PaymentPage;
