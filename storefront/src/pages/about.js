import React, { useContext } from "react";
import BoxTitleSegment from "../components/home/boxTitleSegment";
import Banner from "../components/home/banner";
import BannerText from "../components/about/bannerText";
import {Button, Container, Grid, Header, Image, Segment } from "semantic-ui-react"
import HeaderTitle from "../components/home/headerTitle"
import SegmentLeft from "../components/home/segmentLeft";
import SegmentRight from "../components/home/segmentRight";
import TopHeaderArticle from "../components/home/topHeaderArticle";
import PageContext from "../context/pageContext";

const testListElement = [
  {
    order: 0,
    type: 'holder',
    title: 'test order 0',
    content: 'some test order 0',
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png'
  },
  {
    order: 1,
    type: 'holder',
    title: 'test order 1',
    content: 'some test order 1',
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png'
  },
  {
    order: 2,
    type: 'holder',
    title: 'test order 2',
    content: 'some test order 2',
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png'
  },
  {
    order: 3,
    type: 'banner',
    title: 'my slogan',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tristique enim quis odio hendrerit dictum. Curabitur pellentesque condimentum vestibulum. Donec fermentum ultrices mattis. Curabitur imperdiet interdum hendrerit. Sed finibus semper semper. Cras posuere at augue id pulvinar. Maecenas quis ligula ante. Integer rhoncus cursus tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum est velit, varius in bibendum eu, tempus a lacus. Vivamus eget bibendum ipsum, at dapibus quam. Curabitur blandit, urna vel vehicula dignissim, ipsum massa posuere nibh, vel vehicula mi augue eget ante. Aliquam interdum metus erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tristique enim quis odio hendrerit dictum. Curabitur pellentesque condimentum vestibulum. Donec fermentum ultrices mattis. Curabitur imperdiet interdum hendrerit. Sed finibus semper semper. Cras posuere at augue id pulvinar. Maecenas quis ligula ante. Integer rhoncus cursus tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum est velit, varius in bibendum eu, tempus a lacus. Vivamus eget bibendum ipsum, at dapibus quam. Curabitur blandit, urna vel vehicula dignissim, ipsum massa posuere nibh, vel vehicula mi augue eget ante. Aliquam interdum metus erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'
  }
];



function AboutBuilder(props) {
  const pages = useContext(PageContext);
  const aboutPage = pages.find((page) => page.title == "À propos")

  var banner1 = aboutPage?.PageSettings?.shift()
  var banner2 = aboutPage?.PageSettings?.shift()

  return (        <div>
    <HeaderTitle boxTitle={{...aboutPage, title: aboutPage?.header, hideButton: false}}></HeaderTitle>
        <div >
            <Container>
              <BannerText title={banner1?.title} description={banner1?.description}></BannerText>
            <Segment vertical>
                <Grid container stackable textAlign="center" columns={3}>
                {aboutPage?.PageSettings?.splice(0, 3).map((boxTitle, index) => (
                    <TopHeaderArticle key={index} title={boxTitle.title} description={boxTitle.description} photoURL={boxTitle.imageURL}></TopHeaderArticle>
                    )
                    
              )}
                </Grid>
              </Segment>
              
            </Container>
            <Segment inverted vertical>
              <Container>
              <BannerText title={banner2?.title} description={banner2?.description} inverted></BannerText>
              </Container>
            
        </Segment>
               <Container>
               {aboutPage?.PageSettings?.map((boxTitle, index) => (
                    index%2 == 0 ?
                     (<SegmentLeft key={index} title={boxTitle.title} description={boxTitle.description} photoURL={boxTitle.imageURL}></SegmentLeft>) :
                     (<SegmentRight key={index} title={boxTitle.title} description={boxTitle.description} photoURL={boxTitle.imageURL}></SegmentRight>)
                    )
              )} 
               </Container>
            </div>
            </div>

  );
}

// markup
const AboutPage = ({ data }) => {
  return (
        <AboutBuilder  />
  );
};

export default AboutPage;
