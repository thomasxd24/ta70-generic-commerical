import React, { useContext } from "react";
import * as styles from "../styles/home.module.css";
import * as stylesHelp from "../styles/subscription.module.css";
import FaqElement from "../components/aides/faq-element";
import subscriptionService from "../services/subscription.service";
import {Link, navigate} from "gatsby";
import { Button, Container, Form, Icon, Input, List, Message } from "semantic-ui-react";
import HeaderTitle from "../components/home/headerTitle";
import UserContext from "../context/userContext";

// markupS
const Subscription = ({ data, location }) => {
  const user = useContext(UserContext)
  const offer = location?.state?.offer;

  async function handleSubscribe() {
    subscriptionService.subscribe(offer.id).then((succ) => {
      navigate("/mySubscription");
    }).catch((err) => {
      alert(err);
    })
  }

  return (
        <div>
        <HeaderTitle boxTitle={{title: "S'abonner", hideButton: true }}></HeaderTitle>
        <Container style={{marginTop: '1rem'}}>
          {(!user?.Card || !user?.address || !user?.phone) && <div>
          <Message icon negative>
        <Icon name='warning sign' color="red"/>
        <Message.Content>
        <Message.Header>Votre profil est incomplet</Message.Header>
        Pour continuer l'abonnement, veuillez completez ces informations pour valider votre commande:
        <ul>
        {!user?.Card && <li>Aucune carte n'est enregistrée</li>}
        {!user?.address &&<li>Adresse est manquant</li>}
        {!user?.phone &&<li>Numéro de téléphone est manquant</li>}
        </ul>

        <br></br>
        <Button  as={Link} to="/account/information">Modifier mon profil</Button>
        </Message.Content>
        </Message>
          </div>}
        <div className={stylesHelp.container}>
            <h2 className={stylesHelp.title}>
            Récapitulatif de la commande
            </h2>
            <div >
            <List divided verticalAlign='middle'>
    <List.Item>
      <List.Content floated='right'>
      <h5>{offer?.prices}€</h5>
      </List.Content>
      <List.Content><h3>Abonnement {offer?.title}</h3></List.Content>
    </List.Item>
  </List>
            </div>
            <h3 >
            Prix total à payer (par mois):
            </h3>
            <h2 >
            {offer?.prices}€
            </h2>
          </div>
        <div className={stylesHelp.container}>
            <h2 className={stylesHelp.title}>
            Vos information
            </h2>
            <div >
            <Form>
    <Form.Group widths='equal'>
      <Form.Input fluid label='Nom complet' value={user?.firstName + " " + user?.lastName}  readOnly />
      <Form.Input fluid label='Email' value={user?.email} readOnly />
      <Form.Input fluid label='Numero telephone' value={user?.phone}  readOnly />
    </Form.Group>
    <Form.Group widths='equal'>
      <Form.Input fluid label='Adresse' value={user?.address} readOnly />
    </Form.Group>
  </Form>
            </div>
            <h3 >
            Moyen de paiement
            </h3>
           { user?.Card ? (<div>
            <Form>
    <Form.Group widths='equal'>
        <Form.Field className={styles.inputGroup}>
            <label>Nom sur la carte</label>
            <Input type="text" id="name" value={user?.Card.ownerCard}/>
        </Form.Field>
        <Form.Field className={styles.inputGroup}>
            <label>Numéro de la carte</label>
            <Input id="number" type="number" max="9999999999999999" value={user?.Card.numberCard} />
        </Form.Field>
    </Form.Group>
    <Form.Group widths='equal'>
        <Form.Field className={styles.inputGroup}>
            <label>Code de sécuriter</label>
            <Input fluid id="code" type="password" max="999" value={user?.Card.cvc} />
        </Form.Field>
        <Form.Field className={styles.inputGroup}>
            <label>Date d'expiration</label>
            <Input fluid id="date" type="text" value={user?.Card.expirationDate} />
        </Form.Field>
    </Form.Group>
</Form>
             </div>): (<h5><Icon color='red' name='warning sign' />Aucune carte enregistree</h5>)}
            <Button as={Link} to="/account/information">Modifier mon profil</Button>
          </div>
          <div className={stylesHelp.container}>
            <div>
              <p>
                En validant ce formulaire, vous acceptez <a href="#">les conditions générales de vente.</a>
              <br/>
                Nous plaçons la sécurité et la confidentialité des données personnelles de ses utilisateurs au coeur de ses préoccupations. Découvrez <a
                  href="#">notre politique de protection des données personnelles.</a>
                  <br/>
                  <br/>
              </p>
            </div>
            <Button primary disabled={(!user?.Card || !user?.address || !user?.phone)} large onClick={handleSubscribe}>
            Souscrire à l’abonnement
              </Button>
          </div>
        </Container>
         
        </div>
  );
};

export default Subscription;
