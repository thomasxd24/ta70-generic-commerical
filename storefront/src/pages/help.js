import React, { useContext, useEffect, useState } from "react";
import * as styles from "../styles/home.module.css";
import * as stylesHelp from "../styles/help.module.css";
import FaqElement from "../components/aides/faq-element";
import HeaderTitle from "../components/home/headerTitle";
import { Container } from "semantic-ui-react";
import PageContext from "../context/pageContext";
import pageService from "../services/page.service";
import { set } from "lodash";

// markupS
const HelpPage = ({ data }) => {
  const pages = useContext(PageContext);
  const helpSetting = pages.find(page => page.title == "FAQ")
  const [faqs, setFaq] = useState([])
  useEffect(() => {
    async function fetchFAQ() {
      const response = await pageService.getAllFAQ();
      setFaq(response)
    }
    fetchFAQ()
  },[])

  return (
    <div>
      <HeaderTitle boxTitle={{title: helpSetting?.title, description: helpSetting?.description, hideButton: true }}></HeaderTitle>
      <Container style={{marginTop:"1rem"}}>
        {
          faqs.map((faqObj) =>
            <FaqElement question={faqObj.question} answer={faqObj.answer}/>
          )
        }
          </Container>
    </div>
  );
};

export default HelpPage;
