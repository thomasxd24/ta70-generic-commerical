import React, { useContext } from "react";
import { FaGithub } from "react-icons/fa";
import { graphql } from "gatsby";
import * as styles from "../styles/home.module.css";
import { Link } from "gatsby";
import PanelOffers from "../components/home/panelOffers";
import HeaderTitle from "../components/home/headerTitle";
import { Container } from "semantic-ui-react";

// markup
const OffersPage = ({ data }) => {

    return (
        <div>
            <HeaderTitle boxTitle={{title: "Nos Abonnement", hideButton: true}}></HeaderTitle>
            <Container>
            <PanelOffers/>
            </Container>
            
        </div>
    );
};


export default OffersPage;
