import React, { useContext, useState } from "react";
import {graphql} from "gatsby";
// import * as styles from "../styles/home.module.css";
import NordVpn from "../images/logo-Nordvpn.jpg";
import BoxTitle from "../components/home/boxTitle";
import PanelOffers from "../components/home/panelOffers";
import {Button, Container, Grid, Header, Image, Segment } from "semantic-ui-react"
import HeaderTitle from "../components/home/headerTitle"
import SegmentLeft from "../components/home/segmentLeft";
import SegmentRight from "../components/home/segmentRight";
import TopHeaderArticle from "../components/home/topHeaderArticle";
import PageContext from "../context/pageContext";
const IndexPage = ({data}) => {

  const pages = useContext(PageContext);
  const indexPage = pages.find((page) => page.title == "Acceuil")
    return (
        <div>
<HeaderTitle boxTitle={{...indexPage, title: indexPage?.header, hideButton: false}}></HeaderTitle>
    <div >
        <Container>
        <Segment vertical>
            <Grid container stackable textAlign="center" columns={3}>
            {indexPage?.PageSettings?.splice(0, 3).map((boxTitle, index) => (
                <TopHeaderArticle key={index} title={boxTitle.title} description={boxTitle.description} photoURL={boxTitle.imageURL}></TopHeaderArticle>
                )
          )}
            </Grid>
          </Segment>
          {indexPage?.PageSettings?.map((boxTitle, index) => (
                index%2 == 0 ?
                 (<SegmentLeft key={index} title={boxTitle.title} description={boxTitle.description} photoURL={boxTitle.imageURL}></SegmentLeft>) :
                 (<SegmentRight key={index} title={boxTitle.title} description={boxTitle.description} photoURL={boxTitle.imageURL}></SegmentRight>)
                )
          )}
        </Container>
        <Segment inverted vertical>
      <Grid style={{margin: "0 4rem"}}>
        <Grid.Row textAlign='center'>
            <Grid.Column>
            <Header  style={{fontSize: "3rem", fontWeight: "normal"}} textAlign="center" inverted>
              Vous etes pret?
            </Header>
            </Grid.Column>
        
        </Grid.Row>
        <Grid.Row textAlign='center'>
            <Grid.Column>
            <Button size="huge" primary>Decouvir les offres</Button>
            </Grid.Column>
        
        </Grid.Row>
      </Grid>
    </Segment>
           
        </div>
        </div>

      
    );
};

export const query = graphql`
  query VersionQuery {
    site {
      siteMetadata {
        version
      }
    }
  }
`;

export default IndexPage;
