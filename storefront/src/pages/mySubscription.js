import React, {useContext, useEffect, useState} from "react";
import { FaGithub } from "react-icons/fa";
import { graphql } from "gatsby";
import * as styles from "../styles/home.module.css";
import * as stylesMySubscription from "../styles/my-subscription.module.css";
import { Link } from "gatsby";
import subscriptionService from "../services/subscription.service";
import HeaderTitle from "../components/home/headerTitle";
import { Button, Container, Header, Loader } from "semantic-ui-react";
import UserContext from "../context/userContext";

// markup
const OffersPage = ({ data }) => {

    const user = useContext(UserContext)
    // const text = {
    //     headTitle: "Mon abonnement",
    //     subscriptionType: "Starter",
    //     price: "10,90$/mois",
    //     subscription: "Date de souscription : ",
    //     dateOfSubscription: "01/02/2022",
    //     nextPayement: "Prochain paiement : ",
    //     dateOfNextPayement: "01/03/2022",
    //     avantage1: "Meilleur qualité",
    //     avantage2: "Meilleur débit",
    //     avantage3: "Accès disque ultra rapide",
    //     buttonGoToService: "Accèder au service"
    // }

    // let offer = {
    //     title: "Starter",
    //     prices: "10,90$/mois",
    //     description: "Meilleur qualité\nMeilleur débit\nAccès disque ultra rapide",
    //     dateOfSubscription: "01/02/2022"
    // }

    // const [subscription, setSubscription] = useState(offer);
    // useEffect(() => {
    //     function transformDate(typeDate, inc) {
    //         let date = new Date(typeDate);
    //         if(inc) date.setMonth(date.getMonth()+1);
    //         return date.toLocaleDateString();
    //     }
    //     async function init() {
    //         const offerResult = await subscriptionService.getSubscriptionOfUser();
    //         offerResult.dateOfNextPayement = transformDate(offerResult.dateOfSubscription, true);
    //         offerResult.dateOfSubscription = transformDate(offerResult.dateOfSubscription, false);
    //         setSubscription({...subscription, ...offerResult});
    //     }
    //     init();
    // }, []);

    return (
        <div>
            <HeaderTitle boxTitle={{title: "S'abonner", hideButton: true}}></HeaderTitle>
            { !user ?( <Loader></Loader>) : (<Container>
                <Header size='huge' as="h1" textAlign='center'>Merci.</Header>
                <Header as='h2' textAlign='center'> Votre commande est validé</Header>
                <main className={stylesMySubscription.main}>
                 <div className={stylesMySubscription.hero}>
                 <div className={stylesMySubscription.content}>
                         <div className={stylesMySubscription.informations}>
                             <h2>
                              {user?.Subscription?.title}
                            </h2>
                             <p>
                             <b>Date de souscription :</b><br></br>
                                   {new Date(user?.subscribeAt).toDateString()}
                                 <br/>
                                 <b>Prochain paiement :</b><br></br>
                                   {new Date(user?.unsubscribeAt).toDateString()}
                             </p>
                         </div>
                         <div className={stylesMySubscription.avantages}>
                             <p>
                                 {user?.Subscription?.description}
                             </p>
                         </div>
                         <div className={stylesMySubscription.containerButton}>
                             <Button as={Link} size="huge" to={user?.Subscription?.link} primary>Accèder au service</Button >
                         </div>
                     </div>
                 </div>
             </main>
            </Container>)}
            
        </div>
     
    );
};


export default OffersPage;
