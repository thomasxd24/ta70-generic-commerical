import React, { useContext, useReducer, useState } from "react";
import { FaGithub } from "react-icons/fa";
import { graphql, navigate } from "gatsby";
import { Link } from "gatsby";
import * as styles from "../styles/home.module.css";
import { Container, Card, Input, Button, Form, Icon, Checkbox, Grid, Header, Image, Message, Segment, Divider } from 'semantic-ui-react';
import authService from "../services/auth.service"
// markup
const LoginPage = ({ data }) => {
  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {email: '', password: ''}
  );

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const handleOnChange = event => {
    const { name, value } = event.target;
    setInputValues({ [name]: value });
  };

  const handleLogin = async () => {
  if(loading) return;
    setLoading(true)
    const result =  await authService.signIn(inputValues.email, inputValues.password)
    setLoading(false)
    if(!result)
    {
      console.log("erro")
      setError(true)
      return;
    }
    navigate("/account/information");
  }
  return (
    <div className={styles.container}>
      <main className={styles.main}>
      <Grid textAlign='center' style={{ height: '50vh' }} verticalAlign='middle'>
    <Grid.Column style={{ minWidth: 450 } }>
      <Header as='h2' color='black' textAlign='center'>
         Se connecter
      </Header>
      {error && ( <Message negative>
    <Message.Header> Un erreur est survenu lors de l'authentification</Message.Header>
    <p>Combination Email/mot de passe incorrect</p>
  </Message>)}
     
      <Form size='large'>
        <Segment stacked>
          <Form.Input fluid icon='user' name="email" iconPosition='left' placeholder='E-mail address' value={inputValues.email} onChange={handleOnChange} />
          <Form.Input
            fluid
            icon='lock'
            iconPosition='left'
            placeholder='Password'
            name="password"
            value={inputValues.password}
            onChange={handleOnChange}
            type='password'
          />
          <Button  fluid size='large' onClick={handleLogin} loading={loading}> 
            Se connecter
          </Button>
          <Divider horizontal>Ou</Divider>
          <Button color='black' fluid size='large'  href='https://ta70.ttse.me/api/auth/apple'>
           <Icon name='apple' /> Connexion par Apple
          </Button>
          
          <br></br>
          <Button fluid size='large' href='https://ta70.ttse.me/api/auth/google'>
          <Icon name='google' /> Connexion par Google
          </Button>
          

          
        </Segment>
      </Form>
      <Message>
      Vous n’êtes pas encore inscrit?<Link to="/signup"> Créer un compte</Link>
      </Message>
    </Grid.Column>
  </Grid>
     
      </main>
    </div>
  );
};

export default LoginPage;
