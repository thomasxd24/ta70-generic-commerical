import React, { useContext, useReducer, useState } from "react";
import { FaGithub } from "react-icons/fa";
import { graphql, navigate } from "gatsby";
import { Link } from "gatsby";
import * as styles from "../styles/home.module.css";
import { Container, Card, Input, Button, Form, Icon, Checkbox, Grid, Header, Image, Message, Segment, Divider } from 'semantic-ui-react';
import authService from "../services/auth.service"
// markup
const SignUpPage = ({ data }) => {
  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {firstName: '', lastName: '', email: '', password: ''}
  );

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const handleOnChange = event => {
    const { name, value } = event.target;
    setInputValues({ [name]: value });
  };

  const handleSignup = async () => {
    const result = await authService.signUp(inputValues.firstName, inputValues.lastName, inputValues.email, inputValues.password);
    if(!result)
    {
      setError(true)
      return;
    }
    navigate("/login");
  }
  return (
    <div className={styles.container}>
      <main className={styles.main}>
      <Grid textAlign='center' style={{ height: '50vh' }} verticalAlign='middle'>
    <Grid.Column style={{ minWidth: 450 } }>
      <Header as='h2' color='black' textAlign='center'>
         S'inscrire
      </Header>
      <Form size='large'>
        <Segment stacked>

        <Form.Group widths='equal'>
        <Form.Input fluid icon='user' iconPosition='left' placeholder='Prénom' name="firstName" onChange={handleOnChange} />
        <Form.Input fluid icon='user' iconPosition='left' placeholder='Nom' name="lastName" onChange={handleOnChange}/>
        </Form.Group>
          <Form.Input fluid icon='user' name="email" iconPosition='left' placeholder='E-mail address' onChange={handleOnChange} />
          <Form.Input
          onChange={handleOnChange}
          name="password"
            fluid
            icon='lock'
            iconPosition='left'
            placeholder='Password'
            type='password'
          />
          <Button  fluid size='large' onClick={handleSignup}>
          Créer un compte
          </Button>
          <Divider horizontal>Ou</Divider>
          <Button color='black' fluid size='large'  href='https://ta70.ttse.me/api/auth/apple'>
           <Icon name='apple' /> Connexion par Apple
          </Button>
          
          <br></br>
          <Button fluid size='large' href='https://ta70.ttse.me/api/auth/google'>
          <Icon name='google' /> Connexion par Google
          </Button>
          

          
        </Segment>
      </Form>
      <Message>
      Vous êtes déja inscrit?<a href='#'> Se connecter</a>
      </Message>
    </Grid.Column>
  </Grid>
     
      </main>
    </div>
  );
};

export default SignUpPage;
