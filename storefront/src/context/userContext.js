const { create } = require("lodash-es");
const { useContext, createContext } = require("react");

const UserContext = createContext("Unknown");
export default UserContext;