const { create } = require("lodash-es");
const { useContext, createContext } = require("react");

const PageContext = createContext([]);
export default PageContext;