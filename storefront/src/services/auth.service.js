import instance from "./api.service";

const authService = {
    getCurrentUser: async () => {
        var reponse = await instance.get("/users").then((res) => res.data).catch(()=> null)
        console.log(reponse)
        return reponse;
    },

    signIn: async (email,password) => {
        var reponse = await instance.post("/auth/login", {
            email: email,
            password: password
          }).then((res) => res.data).catch(()=> null)
        console.log(reponse)
        return reponse;
    },
    signUp: async (firstName,lastName,email,password) => {
        var reponse = await instance.post("/auth/signup", {
            firstName,
            lastName,
            email: email,
            password: password
          }).then((res) => res.data).catch(()=> null)
        console.log(reponse)
        return reponse;
    },

    signOff: async (email,password) => {
        var reponse = await instance.get("/auth/logout").then((res) => res.data).catch(()=> null)
        console.log(reponse)
        return reponse;
    }
}

export default authService;
