import instance from "./api.service";

const cardService = {
    getCardUser: async () => {
        var reponse = await instance.get("/users/cards").then((res) => res.data).catch(()=> null)
        console.log(reponse)
        return reponse;
    },

    modifCardUser: async (card) => {
        var reponse = await instance.post("/users/cards/", {...card}).then((res) => res.data).catch(()=> null)
        console.log("service : ",card)
        return reponse;
    },

    deleteCardUser: async () => {
        var reponse = await instance.delete("/users/cards/").then((res) => res.data).catch(()=> null)
        return reponse;
    }
}

export default cardService;