import instance from "./api.service";

const userService = {
    modifUser: async (user) => {
        var reponse = await instance.put("/users/", {...user}).then((res) => res.data).catch(()=> null)
        console.log("service : ",user)
        return reponse;
    },
    deleteUser: async () => {
        var reponse = await instance.delete("/users/").then((res) => res.data).catch(()=> null)
        return reponse;
    }
}

export default userService;