import instance from "./api.service";
// creer un object qui recupère les données du formulaire grace a une fonction appellée au clic du bouton formulaire 
// contactService.postFormData(dataToSend)
 const contactService = { 
    postFormData: async (dataToSend) => {
      var response = await instance.post("/contact", {...dataToSend}).then(
        (res)=>{
          console.log(res);
        }).catch((err)=>{
          console.log(err);
        }
      )
      console.log(response);
      return response;
    }
  }

  export default contactService;