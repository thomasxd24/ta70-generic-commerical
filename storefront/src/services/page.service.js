import instance from "./api.service";

const pageService = {
    getAllPages: async () => {
        var reponse = await instance.get("/pages/").then((res) => res.data).catch(()=> null)
        return reponse;
    },
    getAllFAQ: async () => {
        var reponse = await instance.get("/pages/faq").then((res) => res.data).catch(()=> null)
        return reponse;
    }
}

export default pageService;