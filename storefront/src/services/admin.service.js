import instance from "./api.service";


const adminService = {
    getPages: async () => {
        var reponse = await instance.get("/admin/pages").then((res) => res.data).catch(()=> null)
        return reponse;
    },
    getUsers: async () => {
        var reponse = await instance.get("/admin/users").then((res) => res.data).catch(()=> null)
        return reponse;
    },
    getFAQ: async () => {
        var reponse = await instance.get("/admin/faq").then((res) => res.data).catch(()=> null)
        return reponse;
    },
    getSubscriptions: async () => {
        var reponse = await instance.get("/admin/subscriptions").then((res) => res.data).catch(()=> null)
        return reponse;
    },
    editPage: async (page) => {
        var reponse = await instance.put("/admin/pages/"+page.id, {...page}).then((res) => res.data).catch(()=> null)
        return reponse;
    },

    deletePage: async (pageID) => {
        var reponse = await instance.delete("/admin/pages/"+pageID).then((res) => res.data).catch(()=> null)
        return reponse;
    },
    deleteUser: async (userID) => {
        var reponse = await instance.delete("/admin/users/"+userID).then((res) => res.data).catch(()=> null)
        return reponse;
    },
    addPage: async (page) => {
        var reponse = await instance.post("/admin/pages/",{...page}).then((res) => res.data).catch(()=> null)
        return reponse;
    },
    addUser: async (user) => {
        var reponse = await instance.post("/admin/users/",{...user}).then((res) => res.data).catch(()=> null)
        return reponse;
    },

    addPageSetting: async (page) => {
        var reponse = await instance.post("/admin/pages/settings",{...page}).then((res) => res.data).catch(()=> null)
        return reponse;
    },

    editPageSetting: async (page) => {
        var reponse = await instance.put("/admin/pages/settings/"+page.id, {...page}).then((res) => res.data).catch(()=> null)
        return reponse;
    },

    deletePageSetting: async (pageID) => {
        var reponse = await instance.delete("/admin/pages/settings/"+pageID ).then((res) => res.data).catch(()=> null)
        return reponse;
    },
    addFAQ: async (faq) => {
        var reponse = await instance.post("/admin/faq/",{...faq}).then((res) => res.data).catch(()=> null)
        return reponse;
    },
    deleteFAQ: async (faqID) => {
        var reponse = await instance.delete("/admin/faq/"+faqID).then((res) => res.data).catch(()=> null)
        return reponse;
    },
    addSubscription: async (sub) => {
        var reponse = await instance.post("/admin/subscriptions/",{...sub}).then((res) => res.data).catch(()=> null)
        return reponse;
    },
    deleteSubscription: async (subID) => {
        var reponse = await instance.delete("/admin/subscriptions/"+subID).then((res) => res.data).catch(()=> null)
        return reponse;
    },
    




}

export default adminService;