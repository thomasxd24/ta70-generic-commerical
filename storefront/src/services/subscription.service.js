import instance from "./api.service";

const subscriptionService = {
    getOffers: async () => {
        return await instance.get("/subscription/")
            .then((res) => res.data)
            .catch((err) => console.err(err));
    },

    getSubscriptionOfUser: async () => {
        let reponse = await instance.get("/subscription/get").then((res) => res.data).catch((err) => console.log(err));
        console.log(reponse);
        return reponse;
    },

    subscribe: async (idSubscription) => {
        var reponse = await instance.put(`/subscription/subscribe/${idSubscription}`).then((res) => res.data).catch(()=> null)
        console.log(reponse)
        return reponse;
    },

    unsubscribe: async (idSubscription) => {
        var reponse = await instance.put(`/subscription/unsubscribe`).then((res) => res.data).catch((err) => console.log(err))
        console.log(reponse)
        return reponse;
    }
}

export default subscriptionService;
