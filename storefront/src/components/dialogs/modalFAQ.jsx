import React from 'react'
import { useReducer, useState } from 'react';
import { Button, Header, Icon, Modal, Input, TextArea, Form, Checkbox } from 'semantic-ui-react'
import adminService from '../../services/admin.service';

function ModalFAQ({ handleRefresh}) {
  const [open, setOpen] = useState(false)
  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {question: '', answer: ''}
  );
  const handleOnChange = event => {
    const { name, value } = event.target;
    setInputValues({ [name]: value });
  };
  
  const handleResponse = async (faq) => {
    setOpen(false);
    await adminService.addFAQ(faq);
    handleRefresh();
    setInputValues({question: '', answer: ''})
    
  }

  const handleCancel = () => {
    setOpen(false);
    setInputValues({question: '', answer: ''})
    
  }
  return (
    <Modal
    onClose={() => setOpen(false)}
    onOpen={() => setOpen(true)}
    open={open}
    trigger={(<Button primary >Ajouter une FAQ</Button>)}
  >
      <Header icon>
        <Icon name='newspaper' />
        Ajouter une FAQ
      </Header>
      <Modal.Content>
      <Form>
      <Form.Field
            control={Input}
            label='Question'
            name="question"
            value={inputValues.question}
            onChange={handleOnChange}
          />
          <Form.Field
            control={Input}
            label='Réponse'
            name="answer"
            value={inputValues.answer}
            onChange={handleOnChange}
          />
  </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' onClick={() => handleCancel()}>
          <Icon name='remove' />Annuler
        </Button>
        <Button color='green' onClick={() => handleResponse(inputValues)}>
          <Icon name='checkmark' /> Valider
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default ModalFAQ