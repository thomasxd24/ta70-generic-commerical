import React from 'react'
import { useReducer, useState } from 'react';
import { Button, Header, Icon, Modal, Input, TextArea, Form, Checkbox } from 'semantic-ui-react'
import adminService from '../../services/admin.service';

function ModalSubscription({ handleRefresh}) {
  const [open, setOpen] = useState(false)
  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {title: '', link: '', prices: '', description: ''}
  );
  const handleOnChange = event => {
    const { name, value } = event.target;
    setInputValues({ [name]: value });
  };
  
  const handleResponse = async (subscription) => {
    setOpen(false);
    await adminService.addSubscription(subscription);
    handleRefresh();
    setInputValues({title: '', link: '', prices: '', description: ''})
    
  }

  const handleCancel = () => {
    setOpen(false);
    setInputValues({title: '', link: '', prices: '', description: ''})
    
  }
  return (
    <Modal
    onClose={() => setOpen(false)}
    onOpen={() => setOpen(true)}
    open={open}
    trigger={(<Button primary >Ajouter une Abonnement</Button>)}
  >
      <Header icon>
        <Icon name='newspaper' />
        Ajouter une Abonnement
      </Header>
      <Modal.Content>
      <Form>
      <Form.Field
            control={Input}
            label='Titre'
            name="title"
            value={inputValues.title}
            onChange={handleOnChange}
          />
          <Form.Field
            control={Input}
            label='Lien au service'
            name="link"
            value={inputValues.link}
            onChange={handleOnChange}
          />
          <Form.Field
            control={Input}
            label='Prix'
            name="prices"
            type='number'
            value={inputValues.prices}
            onChange={handleOnChange}
          />
          <Form.Field
            control={TextArea}
            label="Description (Avantage)"
            name="description"
            value={inputValues.description}
            onChange={handleOnChange}
          />
  </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' onClick={() => handleCancel()}>
          <Icon name='remove' />Annuler
        </Button>
        <Button color='green' onClick={() => handleResponse(inputValues)}>
          <Icon name='checkmark' /> Valider
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default ModalSubscription