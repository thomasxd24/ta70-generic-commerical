import React from 'react'
import { useReducer, useState } from 'react';
import { Button, Header, Icon, Modal, Input, TextArea, Form } from 'semantic-ui-react'
import adminService from '../../services/admin.service';

function ModalPage({originalPage, handleRefresh}) {
  const [open, setOpen] = useState(false)
  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    originalPage ?? {title: '', header: '', description: '', path: ''}
  );

  const handleOnChange = event => {
    const { name, value } = event.target;
    setInputValues({ [name]: value });
  };

  const handleResponse = async (page, isEditMode) => {
    setOpen(false);
    if(isEditMode)
    {
    await adminService.editPage(page);
    }
    else {
      await adminService.addPage(page);
    }
    handleRefresh();
    setInputValues(originalPage ?? {title: '', header: '', description: '', path: ''})
    
  }

  const handleCancel = () => {
    setOpen(false);
    setInputValues(originalPage ?? {title: '', header: '', description: '', path: ''})
    
  }
  return (
    <Modal
    onClose={() => setOpen(false)}
    onOpen={() => setOpen(true)}
    open={open}
    trigger={(!originalPage ?( <Button primary >Ajouter une page</Button>) : ( <Button primary >Modifier la page</Button>))}
  >
      <Header icon>
        <Icon name='newspaper' />
        {originalPage ? "Modifer" : "Ajouter"} une page
      </Header>
      <Modal.Content>
      <Form>
      <Form.Field
            control={Input}
            label='Nom'
            name="title"
            value={inputValues.title}
            onChange={handleOnChange}
          />
          <Form.Field
            control={Input}
            label='Titre'
            name="header"
            value={inputValues.header}
            onChange={handleOnChange}
          />
          <Form.Field
            control={TextArea}
            label='Description'
            name="description"
            value={inputValues.description}
            onChange={handleOnChange}
          />
          <Form.Field
            control={Input}
            label="Chemin d'acces"
            name="path"
            value={inputValues.path}
            onChange={handleOnChange}
          />
  </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' onClick={() => handleCancel()}>
          <Icon name='remove' />Annuler
        </Button>
        <Button color='green' onClick={() => handleResponse(inputValues, originalPage != null)}>
          <Icon name='checkmark' /> Valider
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default ModalPage