import React from 'react'
import { useEffect } from 'react';
import { useReducer, useState } from 'react';
import { Button, Header, Icon, Modal, Input, TextArea, Form } from 'semantic-ui-react'
import adminService from '../../services/admin.service';

function ModalPageSetting({originalPage, pageID, handleRefresh}) {
  const [open, setOpen] = useState(false)
  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    originalPage ?? {title: '', description: '', imageURL: '', PageId: pageID}
  );

  useEffect(()=> {
    setInputValues(originalPage ?? {title: '', description: '', imageURL: '', PageId: pageID})
  },[originalPage])

  const handleOnChange = event => {
    const { name, value } = event.target;
    setInputValues({ [name]: value });
  };

  const handleResponse = async (page, isEditMode) => {
    setOpen(false);
    if(isEditMode)
    {
      await adminService.editPageSetting({...inputValues, PageId: pageID});
    }
    else
    {
      await adminService.addPageSetting({...inputValues, PageId: pageID});
    }
    handleRefresh();
    
    setInputValues(originalPage ?? {title: '', description: '', imageURL: '', PageId: pageID})
    
  }

  const handleCancel = () => {
    setOpen(false);
    setInputValues(originalPage ?? {title: '', description: '', imageURL: '', PageId: pageID})
  }

  return (
    <Modal
    onClose={() => setOpen(false)}
    onOpen={() => setOpen(true)}
    open={open}
    trigger={(!originalPage ?( <Button primary >Ajouter un contenu</Button>) : ( <Button icon ><Icon name='edit'></Icon></Button>))}
  >
      <Header icon>
        <Icon name='newspaper' />
        {originalPage ? "Modifer" : "Ajouter"} un contenu
      </Header>
      <Modal.Content>
      <Form>
      <Form.Field
            control={Input}
            label='Nom'
            name="title"
            value={inputValues.title}
            onChange={handleOnChange}
          />
          <Form.Field
            control={TextArea}
            label='Description'
            name="description"
            value={inputValues.description}
            onChange={handleOnChange}
          />
          <Form.Field
            control={Input}
            label="URL de l'image"
            name="imageURL"
            value={inputValues.imageURL}
            onChange={handleOnChange}
          />
  </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' onClick={() => handleCancel()}>
          <Icon name='remove' />Annuler
        </Button>
        <Button color='green' onClick={() => handleResponse(inputValues, originalPage != null)}>
          <Icon name='checkmark' /> Valider
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default ModalPageSetting