import React from 'react'
import { useReducer, useState } from 'react';
import { Button, Header, Icon, Modal, Input, TextArea, Form, Checkbox } from 'semantic-ui-react'
import adminService from '../../services/admin.service';

function ModalUser({ handleRefresh}) {
  const [open, setOpen] = useState(false)
  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {firstName: '', lastName: '', email: '', password: '', address: '', phone: '', admin: false}
  );
  const handleOnChange = event => {
    const { name, value } = event.target;
    setInputValues({ [name]: value });
  };

  const handleOnChangeCheckbox = (e, data) => {
    setInputValues({ admin: data.checked });
  };
  
  const handleResponse = async (user) => {
    setOpen(false);
    await adminService.addUser(user);
    handleRefresh();
    setInputValues({firstName: '', lastName: '', email: '', password: '', address: '', phone: '', admin: false})
    
  }

  const handleCancel = () => {
    setOpen(false);
    setInputValues({firstName: '', lastName: '', email: '', password: '', address: '', phone: '', admin: false})
    
  }
  return (
    <Modal
    onClose={() => setOpen(false)}
    onOpen={() => setOpen(true)}
    open={open}
    trigger={(<Button primary >Ajouter une utilisateur</Button>)}
  >
      <Header icon>
        <Icon name='newspaper' />
        Ajouter une utilisateur
      </Header>
      <Modal.Content>
      <Form>
      <Form.Field
            control={Input}
            label='Nom'
            name="firstName"
            value={inputValues.firstName}
            onChange={handleOnChange}
          />
          <Form.Field
            control={Input}
            label='Prenom'
            name="lastName"
            value={inputValues.lastName}
            onChange={handleOnChange}
          />
          <Form.Field
            control={TextArea}
            label='Email'
            name="email"
            value={inputValues.email}
            onChange={handleOnChange}
          />
          <Form.Field
            control={Input}
            label="Mot de passe"
            type='password'
            name="password"
            value={inputValues.password}
            onChange={handleOnChange}
          />
          <Form.Field
            control={Input}
            label="Numero de telephone"
            name="phone"
            value={inputValues.phone}
            onChange={handleOnChange}
            
          />
           <Form.Field
      control={Checkbox}
      name="admin"
      value={inputValues.admin}
            onChange={handleOnChangeCheckbox}
      label={{ children: 'Administrateur' }}
    />
  </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' onClick={() => handleCancel()}>
          <Icon name='remove' />Annuler
        </Button>
        <Button color='green' onClick={() => handleResponse(inputValues)}>
          <Icon name='checkmark' /> Valider
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default ModalUser