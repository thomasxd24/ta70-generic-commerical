import {Link} from "gatsby";
import React, {useContext, useState} from "react";
import * as styles from "../../styles/help.module.css";
import MedusaLogo from "../../images/medusa-logo.svg";
import {Menu, Segment, Button, Icon} from 'semantic-ui-react'
import { CSSTransition } from 'react-transition-group';

const FaqElement = ({question, answer}) => {
    const [active, setActive] = useState(false);
    const handleItemClick = () => {
        setActive(!active);
    }
    return (
        <div className={styles.faqElement}>
            <div className={styles.rawFAQ}>
                <h3 className={styles.question}>
                    {question}
                </h3>
                <CSSTransition in={active} timeout={1000} classNames="tr">
                    <span className={styles.answer}>
                            {active && <p>{answer}</p>}
                    </span>
                </CSSTransition>
            </div>

            <Icon className={styles.icon} onClick={handleItemClick} name={active ? 'angle left' : 'angle down'}/>
        </div>
    );
};

export default FaqElement;
