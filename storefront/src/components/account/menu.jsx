import React, { useContext } from "react";
import * as styles from "../../styles/account.module.css";
import { Link } from "gatsby";


const Menu = ({selected}) => {
    return (
        <div className={styles.menu}>
            <Link to="/account/information" className={selected == "information" ? styles.linkMarked : styles.link} > Mes informations </Link>
            <Link to="/account/subscription" className={selected == "subscription" ? styles.linkMarked : styles.link}> Mon abonnement </Link>
            <Link to="/account/payment" className={selected == "payment" ? styles.linkMarked : styles.link}> Mes moyens de payments </Link>
            <Link to="/account/invoice" className={selected == "invoice" ? styles.linkMarked : styles.link}> Mes factures </Link>
        </div>
    );
};

export default Menu;