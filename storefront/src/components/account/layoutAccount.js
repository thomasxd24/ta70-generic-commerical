import React, { useContext } from "react";
import * as styles from "../../styles/account.module.css";
import { Link } from "gatsby";
import { Grid, Segment, Menu, Container, Icon, Label } from "semantic-ui-react";
import HeaderTitle from "../home/headerTitle";
import UserContext from "../../context/userContext"

const LayoutAccount = ({children}) => {
  const user = useContext(UserContext);
    const header = {
        title: "Mon Compte",
        hideButton: true
    }
    const activeItem = "bio";
    return (
        <div>
            <HeaderTitle boxTitle={header}></HeaderTitle>
            <Container style={{marginTop: "1rem"}}>
            <Grid>
        <Grid.Column width={4}>
          <Menu fluid vertical tabular>
            <Menu.Item
              name='Mes informations'
              as={Link}
              to="/account/information"
              activeClassName="active"
            >
               {((!user?.phone) || (!user?.address))  && (<Label color="red">!</Label>)}
               Mes informations
            </Menu.Item>
            <Menu.Item
              name='Mon abonnement'
              as={Link}
              to="/account/subscription"
              activeClassName="active"
            />
            <Menu.Item
              name='Mes moyens de payments'
              as={Link}
              to="/account/payment"
              activeClassName="active"
            >
               {!user?.Card  && (<Label color="red">!</Label>)}
               Mes moyens de payments
            </Menu.Item>
            <Menu.Item
              name='Mes factures'
              as={Link}
              to="/account/invoice"
              activeClassName="active"
            />
          </Menu>
        </Grid.Column>  

        <Grid.Column stretched width={12}>
          <Segment>
           {children}
          </Segment>
        </Grid.Column>
      </Grid>
            </Container>
            
            </div>
       
    );
};

export default LayoutAccount;