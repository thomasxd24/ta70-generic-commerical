import React, { useContext } from "react";
import {Header, Segment, Container, Grid, Image} from "semantic-ui-react";

const BoxTitleSegment = (props) => {
  const isRight = props.isRight;
  if (props.isRight) {
    return (
      <Segment>
        <Grid stackable verticalAlign="middle">
          <Grid.Column width={10}>
            <Container fluid>
              <Header as="h2" textAlign="center">
                {props.title}
              </Header>
              <p>{props.content}</p>
            </Container>
          </Grid.Column>
          <Grid.Column width={5}>
            <Container>
              <Image src={props.image} fluid />
            </Container>
          </Grid.Column>
        </Grid>
      </Segment>
    );
  } else {
    return (
      <Segment>
        <Grid stackable verticalAlign="middle">
          <Grid.Column width={5}>
            <Container>
              <Image src={props.image} fluid />
            </Container>
          </Grid.Column>
          <Grid.Column width={10}>
            <Container fluid>
              <Header as="h2" textAlign="center">
                {props.title}
              </Header>
              <p>{props.content}</p>
            </Container>
          </Grid.Column>
        </Grid>
      </Segment>
    );
  }
};

export default BoxTitleSegment;
