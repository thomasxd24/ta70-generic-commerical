import React from "react";
import { Grid, Header } from "semantic-ui-react";


const BoxTitle = ({props}) => {
  return (
    <Grid.Row columns='equal'>
        <Grid.Column>
        {props.imageIsLeft ? (<img src={props.image} style={{ maxHeight: "400px" }} alt="logo" />): 
        ( <div >
          <Header as='h3' style={{ fontSize: '2em' }}>
          {props.title}
            </Header>
            <p style={{ fontSize: '1.33em' }}>
            {props.description}
            </p>
        </div>)}
        </Grid.Column>
        <Grid.Column>
        {props.imageIsLeft ?  ( <div>
    <h1>
                 {props.title}
             </h1>
             <p>
                 {props.description}
             </p>
        </div>): (<img src={props.image} style={{ maxHeight: "400px" }} alt="logo" />)}
        </Grid.Column>
      </Grid.Row>
    // <div className={styles.flexBox}>
    //     {props.imageIsLeft && <img src={props.image} style={{ maxHeight: "400px" }} alt="logo" />}
    //     <div className={styles.titleBox}>
    //         <h1 className={styles.title}>
    //             {props.title}
    //         </h1>
    //         <p className={styles.description}>
    //             {props.description}
    //         </p>
    //     </div>
    //     {!props.imageIsLeft && <img src={props.image} style={{ maxHeight: "400px" }} alt="logo" />}
    // </div>
  );
};

export default BoxTitle;
