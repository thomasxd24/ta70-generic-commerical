import { Link } from "gatsby";
import React, { useContext, useState, useRef, useEffect } from "react";
import * as styles from "../../styles/boxTitle.module.css";
import MedusaLogo from "../../images/medusa-logo.svg";
import {Segment, Container, Header, Button, Icon} from "semantic-ui-react"
import ParticleHeader from "../particle";

const HeaderTitle = ({boxTitle}) => {
  return (
    <Segment
      inverted
      textAlign="center"
      style={{ padding: "1em 0 1.5em" }}
      vertical
    >
      <ParticleHeader></ParticleHeader>
<Container text style={{
        margin: "2em"
      }}>
    <Header
      as="h1"
      content={boxTitle.title}
      style={{
        fontSize:  "3.5em",
        fontWeight: "normal",
        marginBottom: 0
      }}
      inverted
    />
    <Header
      as="h2"
      content={boxTitle.description}
      style={{
        fontSize: "1.5em",
        fontWeight: "normal",
        marginTop: "1em",
        marginBottom: "1em"
      }}
      inverted
    />
    {!boxTitle.hideButton && (<Button primary size="huge" as={Link} to="/offers">
      Decouvir les offres
      <Icon name="right arrow" />
    </Button>)}
    
  </Container>
    </Segment>
  );
};

export default HeaderTitle;
