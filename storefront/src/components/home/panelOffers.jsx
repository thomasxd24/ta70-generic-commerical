import React, {useContext, useEffect, useState} from "react";
import * as styles from "../../styles/boxOffer.module.css";
import { navigate } from "gatsby";
import BoxOffer from "./boxOffer";
import subscriptionService from "../../services/subscription.service";
import HeaderTitle from "./headerTitle";
import { Grid } from "semantic-ui-react";
const PanelOffers = () => {
    const panelOffersConst = {
        offerTitle: "Choisis ton abonnement",
        listOffer: [],
    };
    const [offers, setOffers] = useState(panelOffersConst);
    useEffect(() => {
        async function init() {
            const offers = await subscriptionService.getOffers()
            setOffers({...offers, listOffer: offers})
        }
        init();
    }, []);

    return (
        <Grid columns={3}>
            {offers?.listOffer?.map((offer) => (<Grid.Column>
                        <BoxOffer offer={offer}/>
                        </Grid.Column>
                    ))}
        </Grid>
    );
};

export default PanelOffers;
