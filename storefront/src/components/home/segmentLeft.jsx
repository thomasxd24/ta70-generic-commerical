
import React from "react";
import {graphql} from "gatsby";
// import * as styles from "../styles/home.module.css";
import {Button, Container, Grid, Header, Image, Segment } from "semantic-ui-react"

const SegmentLeft = ({title, description, photoURL}) => {
    return (
        <Segment vertical>
            <Grid stackable>
              <Grid.Column width={10}>
                <Header as="h1">
                  {title}
                </Header>
                <p>
                  {description}
                </p>
              </Grid.Column>
              <Grid.Column width={6}>
                <Image src={photoURL} />
              </Grid.Column>
            </Grid>
          </Segment>
    )
}


export default SegmentLeft;