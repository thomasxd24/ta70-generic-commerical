import React, { useContext } from "react";

import { Card, Divider, Header, List, Button } from "semantic-ui-react";
import BoxTitle from "./boxTitle";

const PageSettingsBuilder = () => {
  return (
    <Grid centered>
      {props.elements.map((element) => (
        <Grid.Row key={element.order}>
            {element.type == "boxTitleSegment" ? (
                <BoxTitleSegment
                title={element.title}
                content={element.content}
                image={element.image}
                isRight={element.order % 2 == 1 ? true : false}
            />
            ) : null}
            {element.type == "boxTitle" ? (
              <BoxTitle title={element.title} />
            ) : null}
            {element.type == "banner" ? (
                <Banner title={element.title} content={element.content} description={element.description} image={element.image}/>
            ) : null}
            {element.type == "boxOffer" ? (
              <p>unimplemented</p>
            ) : null}
            {element.type == "headerTitle" ? (
              <p>unimplemented</p>
            ) : null}
            {element.type == "panelOffer" ? (
              <p>unimplemented</p>
            ) : null}
        </Grid.Row>
      ))}
    </Grid>
  );
};

export default PageSettingsBuilder;
