import React from "react";
import {Segment, Container, Header} from "semantic-ui-react";

const Banner = (props) => {
    return (
        <Segment inverted color="blue">
          <Container fluid>
            <Header as="h2" dividing inverted textAlign='center'>{props.title}</Header>
            <p>{props.content}</p>
          </Container>
        </Segment>
    );
};

export default Banner;