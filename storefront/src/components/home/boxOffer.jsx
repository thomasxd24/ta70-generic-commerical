import React, { useContext } from "react";
import * as styles from "../../styles/boxOffer.module.css";
import {openInEditor} from "../../../.cache/fast-refresh-overlay/utils";
import {navigate} from "gatsby";
import { Button } from "semantic-ui-react";
import UserContext from "../../context/userContext";


const BoxOffer = ({offer}) => {
    const user = useContext(UserContext);
    function handleSubscribe() {
        if(!user)
        {
            navigate("/login")
            return;
        }
        navigate("/subscription", {
            state: {
                offer: offer
            }
        });
    }

    return (
        <div className={styles.offerBox}>
            <h2>{offer.title}</h2>
            <h2>{Number(offer.prices)}</h2>
            <p>{offer.description}</p>
            <Button onClick={handleSubscribe}>Get started</Button>
        </div>
    );
};

export default BoxOffer;
