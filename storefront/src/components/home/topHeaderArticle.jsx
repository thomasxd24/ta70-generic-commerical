
import React from "react";
import {graphql} from "gatsby";
// import * as styles from "../styles/home.module.css";
import {Button, Container, Grid, Header, Image, Segment } from "semantic-ui-react"

const TopHeaderArticle = ({title, description, photoURL}) => {
    return (
      <Grid.Column>
      <Image
        centered
        circular
        size="small"
        src={photoURL}
      />
      <Header as="h1">{title}</Header>
      <p>
       {description}
      </p>
    </Grid.Column>
    )
}


export default TopHeaderArticle;