import { Link } from "gatsby";
import React, { useContext } from "react";
import * as styles from "../../styles/nav-bar.module.css";
import MedusaLogo from "../../images/medusa-logo.svg";

const Footer = () => {
  return (
    <div className={styles.container}>
      <Link to="/" style={{ width: "125px" }}>
        <img src={MedusaLogo} style={{ maxHeight: "40px" }} alt="logo" />
      </Link>
      <div>
        Footer
      </div>
    </div>
  );
};

export default Footer;
