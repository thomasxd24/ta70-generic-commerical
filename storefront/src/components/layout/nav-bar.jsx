import { Link } from "gatsby";
import React, { useContext, useState } from "react";
import * as styles from "../../styles/nav-bar.module.css";
import MedusaLogo from "../../images/medusa-logo.svg";
import { Menu, Segment, Button, Container, Header, Icon } from 'semantic-ui-react'
import { navigate } from "gatsby"
import authService from "../../services/auth.service";
import PageContext from "../../context/pageContext";

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}


const NavBar = ({user}) => {
  const pages = useContext(PageContext)
  const [fixed, setFixed] = useState(false);
  return (

            <Menu
              fixed={fixed ? "top" : null}
              inverted={!fixed}
              pointing={!fixed}
              secondary={!fixed}
              size="large"
            >
              <Container>
                {pages.map((page) => (<Menu.Item as={Link} to={page.path}  activeClassName="active">
                {page.title}
                </Menu.Item>))}
               {user?.Subscription && (<Menu.Item as={Link} to={user.Subscription.link}  activeClassName="active">Acceder au service</Menu.Item>)}
               {user?.admin && (<Menu.Item as={Link} to="/admin/pages"  activeClassName="active">Page Admin</Menu.Item>)}
                {user ? (<Menu.Item position="right">
                <Button as={Link} inverted={!fixed} to="/account/information" >{capitalizeFirstLetter(user.firstName)} {capitalizeFirstLetter(user.lastName)}</Button>
      <Button size='large' onClick={async ()=>{await authService.signOff(); navigate("/")}}  style={{ marginLeft: "0.5em" }} color="red" icon> <Icon name='sign-out' /></Button>
       </Menu.Item>):(<Menu.Item position="right">
    <Button as={Link} inverted={!fixed} to="/login" >
                    Se connecter
                  </Button>
                  <Button
                    as={Link}
                    to="/signup"
                    inverted={!fixed}
                    primary={fixed}
                    style={{ marginLeft: "0.5em" }}
                  >
                    S'inscrire
                  </Button>
    </Menu.Item>)}
              </Container>
            </Menu>
  );
};

export default NavBar;
