import React, { useContext, useEffect, useState } from "react";
import NavBar from "./nav-bar";
import * as styles from "../../styles/layout.module.css";
import "../../styles/globals.css";
import Footer from "./footer";
import authService from "../../services/auth.service";
import { globalHistory } from '@reach/router'
import { Dimmer, Loader, Segment } from "semantic-ui-react";
import { ToastContainer } from 'react-toastify';
import UserContext from "../../context/userContext"
import LayoutAdmin from "./layoutAdmin";
import {useLocation} from "@reach/router"
import PageContext from "../../context/pageContext";
import pageService from "../../services/page.service";
import { navigate } from "gatsby";

const Layout = ({ children }) => {
  const location = useLocation();
  const [user, setUser] = useState(false)
  const [pages, setPages] = useState([])
  useEffect(() => {
    async function fetchUser() {
      var userAPI = await authService.getCurrentUser()
      const paths = window.location.pathname.split("/")[1];
      if(paths == "admin" && !userAPI?.admin) {
        navigate("/", { replace: true })
      }
      setUser(userAPI)
      
    }

    async function fetchPages() {
      setPages(await pageService.getAllPages())
    }
    fetchUser();
    fetchPages();
    globalHistory.listen(({ action }) => {
      if (action === 'PUSH') {
        fetchUser()
        fetchPages();
      }

    })

  }, []);


  return (
    <PageContext.Provider value={pages}>
      <UserContext.Provider value={user}>
        {
         ( pages.length == 0) || (user == false)   ? ( <Dimmer blurring inverted active>
            <Loader>Loading</Loader>
          </Dimmer>) : ( location.pathname.split("/")[1] != "admin" ? (<main>
            <Segment
              inverted
              textAlign="center"
              style={{ padding: "1em 0 1.5em" }}
              vertical
            ><NavBar user={user} /></Segment>{children}
          </main>) : (<LayoutAdmin>
    {children}
  </LayoutAdmin>))
        }



        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <ToastContainer />
      </UserContext.Provider>

    </PageContext.Provider>

  )
};

export default Layout;
