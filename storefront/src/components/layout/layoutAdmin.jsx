
import React from "react";
import {graphql, Link} from "gatsby";
// import * as styles from "../styles/home.module.css";
import {Button, Container, Grid, Header, Image, Menu, Segment } from "semantic-ui-react"
import { useContext } from "react";
import UserContext from "../../context/userContext";

const LayoutAdmin = ({children}) => {
  const user = useContext(UserContext)
    return (
        <div style={{height: "100%"}}>
            <Menu borderless inverted fluid fixed="top">
            <Menu.Item header as="a">
              TA70
            </Menu.Item>
            <Menu.Menu position="right">
              <Menu.Item as={Link} to="/">Retour au application</Menu.Item>
              <Menu.Item>Bonjour, {user?.firstName} {user?.lastName}</Menu.Item>
            </Menu.Menu>

          </Menu>
          <Grid style={{paddingTop: "4rem"}}>
        <Grid.Column width={4}>
          <Menu fluid vertical size="huge" tabular>
            <Menu.Item
            as={Link}
            to="/admin/pages"
              name='Pages \/ Contenu des pages'
              activeClassName="active"
            />
            <Menu.Item
            as={Link}
            to="/admin/faq"
              name='FAQ'
              activeClassName="active"
            />
            <Menu.Item
            as={Link}
            to="/admin/subscriptions"
              name='Abonnements'
              activeClassName="active"
            />
            <Menu.Item
            as={Link}
            to="/admin/users"
              name='Utilisateurs'
              activeClassName="active"
            />
          </Menu>
        </Grid.Column>

        <Grid.Column stretched width={12}>
          <Segment>
           {children}
          </Segment>
        </Grid.Column>
      </Grid>
           
          </div>
    )
}


export default LayoutAdmin;