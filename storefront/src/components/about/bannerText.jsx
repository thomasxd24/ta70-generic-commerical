import React from "react";
import { Header } from "semantic-ui-react";

const BannerText = ({title, description, inverted}) => {
    return (<div style={{margin: "2rem 0"}}>
        <Header size="huge" textAlign="center" inverted={inverted}>{title}</Header>
        <p style={{textAlign: "center"}}>{description}</p>
      </div>)

}


export default BannerText;