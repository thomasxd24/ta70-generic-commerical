import React from "react";
import Layout from "./src/components/layout/layout";
import 'semantic-ui-css/semantic.min.css'
import 'react-toastify/dist/ReactToastify.css';


export const wrapPageElement = ({ element }) => {
  return (
         <Layout>{element}</Layout>
  );
};
