# TA70 Generic Commercial

## Pré-requis
- [Installer NodeJS]("https://nodejs.org/en/download/")
- [Installer Docker (Ubuntu/Linux)]("https://doc.ubuntu-fr.org/docker")
- [Installer Docker (MacOS)]("https://docs.docker.com/desktop/mac/install/")
- [Installer Docker Desktop (Windows)]("https://www.docker.com/get-started")

## Contexte
Le contexte du projet survient de par une problématique répandue au sein d’entreprises qui souhaitent proposer des services en ligne. En effet, certaines entreprises ont besoin de rendre accessible leurs services auprès de potentiels clients.

Une des solutions peut être de proposer un site qui met en valeur les services et qui permet à tout prospect de s'abonner à ceux-ci. Par conséquent, nous avons choisi de répondre à ce besoin en proposant à ces entreprises l’application développée dans ce projet.

Au-delà du besoin, ce projet est un exercice qui intervient de le cadre pédagogique de notre formation d'ingénieur en systèmes d’information à l’UTBM. 

Pour plus d'informations sur la définition, les choix et techniques l'architecture, vous pouvez aller consulter le [rapport de projet]("https://docs.google.com/document/d/17y3gpnNqJWl5xGX9gTj57PV1Gmkt9SEIrPxlIvu9Y54/edit?usp=sharing")

## Installation
Pour lancer le projet, il faut installer les dépendances et l'executer
### Storefront
Rentrer dans le dossier storefront
- npm install
- npm run start
### Backend
Rentrer dans le dossier backend
- npm install
- npm run start

## Utilisateur

Par defaut, il y a un compte admin deja créé:

- Email: admin@utbm.fr
- Mdp: admin


## Architecture

Ce projet est découpée en deux parties :
- Le storefront, c’est la vitrine de l'application, la partie visible par l’utilisateur. 
  - Développé en ReactJS avec le framework Gastby
- Le backend, C’est une API qui interagit avec le Storefront et le modèle de données.
  - Développé en NodeJS et Sequelize

