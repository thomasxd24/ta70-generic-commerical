#backend
FROM node:latest
ENV NODE_ENV=development

WORKDIR /app

ENV GATSBY_API_URL=/api

COPY ["./", "/app/"]
WORKDIR "/app/storefront/"
RUN npm install
RUN npm run build

WORKDIR "/app/backend/"
RUN npm install




COPY . . 
EXPOSE 3000
CMD ["node", "bin/www"]
